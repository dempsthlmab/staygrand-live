import React from 'react';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute } from 'react-router';
import configureStore from 'store/configureStore';

import App from 'containers/App';
import Welcome from 'containers/Welcome';
import SingleHotel from 'containers/SingleHotel';
import About from '../components/layout/Overlays/About';

export default function(history) {
    history.listen(function (location) {
        if (!process.env.ON_SERVER) {
            ga('set', 'page', location.pathname + location.search);
            ga('send', 'pageview');
        }
    });

  return (
    <Router history={history}>
      <Route path="/" component={App}>
        <Route path="city/:slug" component={Welcome} searchType="city" />
        <Route path="area/:slug" component={Welcome} searchType="area" />
        <Route path="country/:slug" component={Welcome} searchType="country" />
        <Route path="continent/:slug" component={Welcome} searchType="continent" />
        <Route path="hotel/:slug" component={Welcome} searchType="hotel" />
        <Route path="/About" component={About}/>
        <IndexRoute component={Welcome} />
      </Route>
    </Router>
  )
}
