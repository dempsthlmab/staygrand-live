import Immutable from 'immutable';

let defaultState = Immutable.fromJS({
    hotels: [],
    loadingMore: false,
    loadingNew: false,
    totalHotels: null,
    next_page: '',
    error: null,
});

export default function(state = defaultState, action) {
    switch(action.type) {
        case 'GET_INIT_HOTELS':
            return state.merge({
                hotels: [action.res.data],
                totalHotels: action.res.total,
                next_page: action.res.nextPageUrl
            });

        case 'GET_SEARCH_FILTERED_HOTELS':
            return state.merge({
                hotels: [action.res1.data],
                totalHotels: action.res1.total,
                next_page: action.res1.nextPageUrl,
                loadingNew: false
            });

        case 'GET_SEARCH_FILTERED_HOTELS_REQUEST':
            return state.merge({ loadingNew: true });

        case 'GET_MORE_HOTELS':
            return state.merge({
                hotels: [...state.get('hotels'), action.res.data],
                next_page: action.res.nextPageUrl,
                loadingMore: false
            });

        case 'GET_MORE_HOTELS_REQUEST':
            return state.merge({ loadingMore: true });

        case 'GET_INIT_HOTELS_FAILURE':
            return state.merge({ error: action.error });

        case 'GET_FILTER_HOTELS':
            return state.merge({
                hotels: [action.res1.data],
                totalHotels: action.res1.total,
                next_page: action.res1.nextPageUrl,
                loadingNew: false
            });

        case 'GET_FILTER_HOTELS_REQUEST':
            return state.merge({ loadingNew: true });

        case 'RESET_INIT_HOTELS':
            return state.merge({
                hotels: [action.res1.data],
                totalHotels: action.res1.total,
                next_page: action.res1.nextPageUrl,
                loadingNew: false
            });

        case 'RESET_INIT_HOTELS_REQUEST':
            return state.merge({ loadingNew: true });

        case 'GET_ORDERED_HOTELS':
            return state.merge({
                hotels: [action.res.data],
                next_page: action.res.nextPageUrl,
                loadingNew: false
            });

        case 'GET_ORDERED_HOTELS_REQUEST':
            return state.merge({ loadingNew: true });


        default:
      return state;
  }
}
