import Immutable from 'immutable';

let defaultState = Immutable.fromJS({
    filters: {
        ratings: [],
        claimtofames: []
    },
    activeFilter:{
        ratings: [],
        claimtofames: []
    },
    search: {
        searchCategorys: null,
        searchIcon: 'default',
        searchFilter: {},
        searchVal: '',
    },
    activeOrder: null,
    activeHotel: null,
    view: 'Box',
    error: null,
});

export default function(state = defaultState, action) {
    switch(action.type) {
        case 'GET_INIT_STATE':
            return state.merge({
                filters: {
                    ratings: action.res.ratings,
                    claimtofames: action.res.claimtofames
                }
            });

        case 'GET_ACTIVE_FILTERS':
            return state.merge({
                activeFilter: {
                    ratings: action.res3.ratings,
                    claimtofames: action.res3.claimtofames
                }
            });

        case 'RESET_FILTERS':
            return state.merge({
                activeFilter: {
                    ratings: [],
                    claimtofames: []
                }
            });

        case 'SET_ACTIVE_LOCATION_FILTER':
            let searchVal = action.res3.get('val').replace('+', ' ');

            if (action.res3.get('location') === 'hotel') {
                if (action.res1.data.length === 1) {
                    searchVal = action.res1.data[0].name;
                }
            }

            return state.merge({
                search: {
                    searchCategorys: state.getIn(['search', 'searchCategorys']),
                    searchIcon: state.getIn(['search', 'searchIcon']),
                    searchFilter: action.res3,
                    searchVal: searchVal
                }
            });

        case 'SEARCH_RESULT':
            return state.merge({
                search: {
                    searchCategorys: action.res,
                    searchIcon: 'done',
                    searchFilter: state.getIn(['search', 'searchFilter']),
                    searchVal: state.getIn(['search', 'searchVal'])
                }
            });

        case 'SEARCH_RESULT_REQUEST':
            return state.merge({
                search: {
                    searchCategorys: state.getIn(['search', 'searchCategorys']),
                    searchIcon: 'loading',
                    searchFilter: state.getIn(['search', 'searchFilter']),
                    searchVal: state.getIn(['search', 'searchVal'])
                }
            });

        case 'RESET_SEARCH_FILTER':
            return state.merge({
                search: {
                    searchCategorys: null,
                    searchIcon: 'default',
                    searchFilter: {},
                    searchVal: ''
                }
            });

        case 'SEARCH_RESULT_FAILURE':
            return state.merge({ error: action.error });


        case 'TOGGLE_START_VIEW':
            return state.merge({ view: action.res });

        case 'SET_ACTIVE_HOTEL':
            return state.merge({
                activeHotel: action.res,
                view: 'Map',
            });

        case 'RESET_ACTIVE_HOTEL':
            return state.merge({
                activeHotel: null,
            });

        case 'SET_ACTIVE_ORDER':
            return state.merge({
                activeOrder: action.order,
            });

        case 'SET_DEFAULT_SEARCH':
        return state.merge({
            search: {
                searchCategorys: null,
                searchIcon: 'default',
                searchFilter: {},
                searchVal: '',
            }
        });

        default:
      return state;
  }
}
