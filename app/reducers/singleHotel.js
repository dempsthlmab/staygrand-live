import Immutable from 'immutable';

let defaultState = Immutable.fromJS({
    singleHotel: [],
    ratings: [],
    error: null
});

export default function(state = defaultState, action) {
    switch(action.type) {
        case 'GET_SINGLE_HOTEL':
            // console.log(action.response);
            return state.merge({
                singleHotel: action.res.resp1,
                ratings: action.res.resp2
            });

        case 'GET_SINGLE_HOTEL_FAILURE':
            return state.merge({ error: action.error });

        default:
      return state;
  }
}
