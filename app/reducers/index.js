import { combineReducers } from 'redux';
import hotels from 'reducers/hotels';
import singleHotel from 'reducers/singleHotel';
import mapHotels from 'reducers/mapHotels';
import siteState from 'reducers/siteState';

const rootReducer = combineReducers({
  hotels,
  singleHotel,
  mapHotels,
  siteState,
});

export default rootReducer;
