import Immutable from 'immutable';

let defaultState = Immutable.fromJS({
    hotels: [],
    loadingNew: false,
    error: null,
    popupHotel: null
});

export default function(state = defaultState, action) {
    switch(action.type) {
        case 'GET_ALL_MAP_HOTELS':
            return state.merge({
                hotels: action.res.data,
                loadingNew: false
            });

        case 'GET_ALL_MAP_HOTELS_REQUEST':
            return state.merge({
                loadingNew: true,
            });

        case 'GET_ALL_MAP_HOTELS_FAILURE':
            return state.merge({ error: action.error });



        case 'GET_MAP_HOTELS_IN_BOUNDS':
            return state.merge({
                hotels: action.res,
                loadingNew: false
            });
        case 'GET_MAP_HOTELS_IN_BOUNDS_REQUEST':
            return state.merge({
                true: false
            });

        case 'GET_POPUP_HOTEL':
            return state.merge({
                popupHotel: action.res,
            });
        case 'RESET_POPUP_HOTEL':
            return state.merge({
                popupHotel: null,
            });

        case 'GET_SEARCH_FILTERED_HOTELS_MAPS':
            return state.merge({
                hotels: action.res2.data,
                loadingNew: false
            });

        case 'GET_SEARCH_FILTERED_HOTELS_MAPS_REQUEST':
            return state.merge({
                loadingNew: true,
            });


        case 'GET_FILTER_HOTELS_MAPS':
            return state.merge({
                hotels: action.res2.data,
                loadingNew: false
            });

        case 'GET_FILTER_HOTELS_MAPS_REQUEST':
            return state.merge({
                loadingNew: true,
            });

        case 'RESET_INIT_HOTELS_MAPS':
            return state.merge({
                hotels: action.res2.data,
                loadingNew: false
            });

        case 'RESET_INIT_HOTELS_MAPS_REQUEST':
            return state.merge({
                loadingNew: true,
            });

        default:
            return state;
  }
}
