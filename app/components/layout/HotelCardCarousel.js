/*
HotelCardCarousel is the component that shows an image carousel of the hotel cards on the home page.
*/

import React, { Component } from "react";
import Slider from "react-slick";

export default class HotelCardCarousel extends Component {

    /*
    The first render of this component will not include any images so that is why we need
    to make a function that checks if there is any images.
    */
    loadImages(images, settings){
        if(images !== null){
            return(
                <Slider className="center" ref="slider" {...settings}>
                    {images}
                </Slider>
            );
        }else{
            return <p style={{'display': 'none'}}></p>;
        }
    }
    /*
    When the user clicks on the close button the parent component hideHotelImgCarousel function will be fired
    and we will set the start position of the carousel to the first image.
    */
    handleClose(){
        this.props.hideHotelImgCarousel();
        this.refs.slider.slickGoTo(0);
    }

    render(){
        const settings = {
          dots: true,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px"
        };
        const hotelImages = this.props.hotelImages;
        let images;
        if(hotelImages.length > 0){
            images = hotelImages.map((image, index) => {
                // let image_url = image.get('imageUrl');
                // if(image_url.substring(0, 4) !== 'http'){
                //     image_url = 'https://www.staygrand.com'+image_url;
                // }
                return <div key={index}><div className="hotel-card-carousel-item" style={{backgroundImage: "url("+image+")", backgroundSize: "cover"}}></div></div>
            });
        }else{
            images = null;
        }

        return(
            <div className={this.props.showCarousel ? "hotel-card-carousel active" : "hotel-card-carousel"}>
                <div className="hotel-card-carousel-slider-wrapper">
                    <div className="hotel-card-carousel-slider">
                        { this.loadImages(images, settings) }
                        <div className="close-carousel-wrapper">
                            <button onClick={(event) => { this.handleClose(); }}>Close View</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


/*
    function(i) {
        return <a><img src={`${baseUrl}/abstract0${i+1}.jpg`}/></a>
      }
*/
