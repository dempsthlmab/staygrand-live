import React, { Component } from "react";

import OverlayGallery from "./OverlayGallery";
import HotelHero from "./HotelHero";

export default class HeroComponent extends Component{
    constructor(){
        super();
        this.state = {
            galleryOpen: false,
            windowW: 0,
        };
        this.onResize = this.updateDimensions.bind(this);
    }
    updateDimensions() {

        this.setState({ windowW: window.innerWidth });
    }
    componentWillMount() {
        if(typeof window !== 'undefined'){
            this.updateDimensions();
        }
    }
    componentDidMount() {
        window.addEventListener("resize", this.onResize, false);
    }
    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize, false);
    }
    toggleGallery(){
        // console.log(this.state.gall);
        if(this.state.galleryOpen === false){
            this.setState({galleryOpen: true});
        }else{
            this.setState({galleryOpen: false});
        }
    }
    render(){
        return(
            <div>
                <HotelHero Hotel={this.props.Hotel} toggleGallery={this.toggleGallery.bind(this)} windowW={this.state.windowW}/>
                <OverlayGallery Images={this.props.Images} openState={this.state.galleryOpen} toggleGallery={this.toggleGallery.bind(this)}/>
            </div>
        );
    }
}
