import React, { Component } from 'react';
import GoogleMap from 'google-map-react';
import MapMarker from '../MapViewParts/MapMarker';

export default class HotelMiniMap extends Component {

    enableScrollOnMap(e){
        if(e.touches.length < 2){
            e.stopPropagation();
        }
    }
    render() {
      const defaultVals = {
        minZoomOverride: false,
        zoom: 15,
        minZoom: 3,
        maxZoom: 17,
        options: {
            panControl: false,
            mapTypeControl: false,
            scrollwheel: false,
            gestureHandling: 'cooperative',
            styles: [
                { elementType: 'geometry', stylers: [{color: '#ebebeb'}] },
                { elementType: 'labels.text.stroke', stylers: [{color: '#ebebeb'}] },
                { elementType: 'labels.text.fill', stylers: [{color: '#000000'}] },
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#000000'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#000000'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#c5c5c5'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#c5c5c5'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#ffffff'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#000000'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#dadada'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#dadada'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#000000'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#dadada'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#dadada'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ]
        },
      };
          const hotel = this.props.Hotel;
          const HotelMarkers = <MapMarker
                    key={hotel.get('hotelId')}
                    lat={hotel.get('lat')}
                    lng={hotel.get('lng')}
                    name={hotel.get('name')}
                    avgRating={hotel.get('avgRating')}
                    numAwards={hotel.get('numAwards')}
                    numOpinions={hotel.get('numOpinions')}
                    cityPlace={hotel.get('cityPlace')}
                    country={hotel.get('country')}
                />;
            let lat = hotel.get('lat'),
                lng = hotel.get('lng');
            // lat = lat + 0.002811;
            const center = {lat: lat, lng: lng};
      return (
          <div className="single-hotel-map-view" onTouchMove={(event) => {this.enableScrollOnMap(event);}}>
            <GoogleMap
              bootstrapURLKeys={{
                 key: 'AIzaSyDdvkleY5cNjvxO4cTj-Vn1toU4uDNPI0w',
                 language: 'en',
              }}
              options={defaultVals.options}
              draggable={false}
              defaultCenter={center}
              defaultZoom={defaultVals.zoom}
              defaultMinZoom={defaultVals.minZoom}
              defaultMaxZoom={defaultVals.maxZoom}
              defaultMinZoomOverride={defaultVals.minZoomOverride}>
                {HotelMarkers}
            </GoogleMap>
          </div>
      );
   }
}
