import React, { Component } from "react";

import OpinionItem from "./Items/OpinionItem";

export default class HotelMedia extends Component {

    opinionsRender(media, opinionItems){
        if(media > 0){
            return(
                <div className="media-section-body-wrapper">
                    <div className="containery">
                        <div className="media-section-body">
                            { opinionItems }
                        </div>
                    </div>
                </div>
            );
        }
    }

    render(){
        const hotelOpinionData = this.props.opinions;
        const opinionItems = hotelOpinionData.map((hotelOpinion) => {
            return <OpinionItem key={hotelOpinion.get('opinionId')} content={hotelOpinion.get('opinion')} date={hotelOpinion.get('opinionDate')} source={hotelOpinion.get('source')} link={hotelOpinion.get('sourceLink')} />
        });
        return(
            <div className="media-section">
                <div className="containery">
                    <div className="media-section-header-wrapper">
                        <div className="media-section-header">
                            <div className="section-1">
                                <div className="score"><p>{this.props.totalMedia}</p></div>
                            </div>
                            <div className="section-2">
                                <div className="section-icon"></div>
                                <div className="section-title">{"Media Opinions & Comments"}</div>
                            </div>
                        </div>
                    </div>
                </div>
                { this.opinionsRender(this.props.totalMedia, opinionItems) }
            </div>
        );
    }

}
