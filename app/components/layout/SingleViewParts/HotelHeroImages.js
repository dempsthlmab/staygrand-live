import React, { Component } from "react";

export default class HotelSlider extends Component {
    constructor() {
        super();
        this.state = {
            trackStyle:  {
                width: 0,
                transform: `translate3d(0px, 0px, 0px)`,
            },
            slideWidth: {
                width: 0,
            },
        };
        this.onResize = this.updateDimensions.bind(this);
    }

    updateDimensions() {
        let wWidth = window.innerWidth;
        if(wWidth > 769){
            wWidth -= 120;
        }
        this.setState({ trackStyle: { transform: `translate3d(-${wWidth}px, 0px, 0px)`, width: (wWidth * 3)+"px" }, slideWidth: {width: wWidth+"px"} });
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.onResize, false);
    }

    /**
    * Remove event listener
    */
    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize, false);
    }

    render(){
        const { Images } = this.props;
        let secondImg = '';
        let hotelImages = '';
        if(Images.size === 2){
            secondImg = Images.get(1);
            hotelImages = Images.push(secondImg);
        }else{
            hotelImages = Images;
        }
        const heroImages = hotelImages.map((image, index) => {
            let image_url = image.get('imageUrl');
            if(image_url.substring(0, 4) !== 'http'){
                image_url = 'https://www.staygrand.com'+image_url;
            }
            return <div key={index} className="Stat is-yellow js-stat" style={this.state.slideWidth}><div className="hotel-slider-item" style={{backgroundImage: "url("+image_url+")", backgroundSize: "cover"}}><div className="hotel-slider-item-overlay"><div className="border"></div></div></div></div>
        });
        const centerImg = heroImages.get(0);
        const restImages = heroImages.shift(0);
        const newImage = restImages.insert(1, centerImg);
        return(
            <div className="Holder">
                <div className="Stats">
                    <div className="Stats-list js-stats" style={this.state.trackStyle}>
                        { newImage }
                    </div>
                </div>
            </div>
        );
    }
}

// centerPadding: "60px",
