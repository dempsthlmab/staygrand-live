import React, { Component } from "react";
import Slider from "react-slick";

export default class OverlayGallery extends Component {
    render(){
        const settings = {
          dots: true,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px"
        };
        const hotelImages = this.props.Images;
        const images = hotelImages.map((image, index) => {
            let image_url = image.get('imageUrl');
            if(image_url.substring(0, 4) !== 'http'){
                image_url = 'https://www.staygrand.com'+image_url;
            }
            return <div key={index}><div className="overlay-hotel-slider-item" style={{backgroundImage: "url("+image_url+")", backgroundSize: "cover"}}></div></div>
        });
        return(
            <div className={this.props.openState ? "overlay-hotel-slider-box active" : "overlay-hotel-slider-box"}>
                <div className="overlay-hotel-slider-wrapper">
                    <div className="overlay-hotel-slider">
                        <Slider className="center" {...settings}>
                            {images}
                        </Slider>
                        <div className="close-gallery-wrapper">
                            <button onClick={this.props.toggleGallery}>Close View</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
