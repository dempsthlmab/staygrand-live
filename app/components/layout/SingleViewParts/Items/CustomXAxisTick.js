import React, { Component } from "react";

export default class CustomXAxisTick extends Component {

    render(){
        const {x, y, stroke, payload} = this.props;
        return(
            <g transform={`translate(${x},${y})`}>
                <text x={-10} y={5} dy={0} textAnchor="end" fill="#fff" transform="rotate(-90)">{payload.value}</text>
            </g>
        );
    }
}
// {this.props.reviewFor}
