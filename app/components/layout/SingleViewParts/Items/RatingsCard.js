import React, { Component } from "react";
import SmallScoreCircle from "./SmallScoreCircle";
export default class RatingsCard extends Component {
    render(){
        const data = this.props;
        return(
            <div className={"ratings-card "+data.info.number}>
                <div className="ratings-card-header">
                    <div className="card-score">
                        <SmallScoreCircle score={data.data.get('rating')} />
                    </div>
                    <div className="type-wrapper">
                        <div className="type">
                            <p className="main"><span>{data.info.main+": "}</span>{data.catVal}</p>
                        </div>
                    </div>
                </div>
                <div className="ratings-card-body">
                    <p>{data.data.get('note')}</p>
                </div>
            </div>
        );
    }
}
// {data.info.name}
