import React, { Component } from "react";

export default class CustomYAxisTick extends Component {

    render(){
        const {x, y, stroke, payload} = this.props;
        const yAxisTexts = ["Not acceptable", "Acceptable", "Good", "Really Good", "Fantastic", "World Elite", "Staygrand World Best"];

        return(
            <g transform={`translate(${x},${y})`}>
                <text x={-10} y={5} dy={0} textAnchor="end" fill={payload.value === 10 ? "#d1beab" : "#fff"}>{payload.value === 0 ? yAxisTexts[payload.value] : yAxisTexts[payload.value -5]}{" "}{payload.value}</text>
            </g>
        );
    }
}
