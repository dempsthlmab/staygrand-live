import React, { Component } from "react";

export default class CtfNoNoteItem extends Component {

    render(){
        const { ctfs } = this.props;
        const types = ctfs.map((ctf, index) =>{
            return (
                <div key ={index} className="ctf-no-note-item">
                        <p>{ctf.claimToFameType.description}</p>
                </div>
            );
        });
        return(
            <div className="ctf-no-note-item-wrapper">
                { types }
            </div>
        );
    }
}
// {this.props.reviewFor}
