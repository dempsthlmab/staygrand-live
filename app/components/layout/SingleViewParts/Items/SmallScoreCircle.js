import React, { Component } from "react";

export default class SmallScoreCircle extends Component {
    polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        const angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }
    describeArc(x, y, radius, startAngle, endAngle, stroker){

        const start = this.polarToCartesian(x, y, radius, endAngle);
        const end = this.polarToCartesian(x, y, radius, startAngle);

        const largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

        const d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");

        return (<path id="arc1" fill="none" stroke={stroker} strokeWidth="3" d={d}></path>);
    }
    render(){
        let score =  3.6 * (this.props.score * 10);
        if(score === 360){
            score = score - 1;
        }
        let stroker;
        if(score === 0){
            stroker = "rgba(255,255,255,0)";
        }else{
            stroker = "#fff";
        }
        return(
            <div className="small-score-circle">
                <div className="small-score-circle__text">
                    <div className="small-score-circle__text__primary">
                        {this.props.score}
                    </div>
                </div>
                <div className="small-score-circle__circle">
                    <svg>
                        { this.describeArc(30, 30, 28, 0, score, stroker) }
                    </svg>
                </div>
            </div>
        );
    }
}
