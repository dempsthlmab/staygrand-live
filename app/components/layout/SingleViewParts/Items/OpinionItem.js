import React, { Component } from "react";


const Item = (props) => {
    if(props.link !== ''){
        return (
            <a href={props.link} target="_blank">
                <div className="opinion-item-wrapper media-item link">
                    <div className="opinion-item">
                        <div className="opinion-item-header">
                            <p className="source">{props.source}</p>
                        </div>
                        <div className="opinion-item-body">
                            <p className="content">{props.content}</p>
                        </div>
                        <div className="opinion-item-footer">
                            <p className="date">{props.date}</p>
                        </div>
                    </div>
                </div>
            </a>
        );
    }else{
        return(
            <div className="opinion-item-wrapper media-item no-link">
                <div className="opinion-item">
                    <div className="opinion-item-header">
                        <p className="source">{props.source}</p>
                    </div>
                    <div className="opinion-item-body">
                        <p className="content">{props.content}</p>
                    </div>
                    <div className="opinion-item-footer">
                        <p className="date">{props.date}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default class OpinionItem extends Component {

    render(){
        const Opinion = this.props;
        return(
            <Item source={Opinion.source} link={Opinion.link} content={Opinion.content} date={Opinion.date} />
        );
    }
}
// <p className="date"><span>{"Date: "}</span>{Opinion.date}</p>

// <Source source={Opinion.source} link={Opinion.link} />
