import React, { Component } from "react";

export default class CtfItem extends Component {

    render(){
        const { note, type } = this.props;
        return(
            <div className="ctf-note-item-wrapper">
                <div className="ctf-note-item">
                    <div className="ctf-note-item-header">
                        <p>{type}</p>
                    </div>
                    <div className="devider"></div>
                    <div className="ctf-note-item-footer">
                        <p>{note}</p>
                    </div>
                </div>
            </div>
        );
    }
}
// {this.props.reviewFor}
