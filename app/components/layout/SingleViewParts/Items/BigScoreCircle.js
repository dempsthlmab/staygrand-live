import React, { Component } from "react";

export default class BigScoreCircle extends Component {
    polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        const angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }
    describeArc(x, y, radius, startAngle, endAngle, stroker){

        const start = this.polarToCartesian(x, y, radius, endAngle);
        const end = this.polarToCartesian(x, y, radius, startAngle);

        const largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

        const d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, largeArcFlag, 0, end.x, end.y
        ].join(" ");

        return (<path id="arc1" fill="none" stroke={stroker} strokeWidth="3" d={d}></path>);
    }
    render(){
        let score =  3.6 * (this.props.score * 10);
        let stroker;
        let circleVals;
        if(score === 360){
            score = score - 1;
        }

        if(this.props.score === null){
            stroker = "rgba(255,255,255,0)";
        }else{
            stroker = this.props.stroke;
        }
        if(this.props.windowW < 770){
            circleVals = {
                center: 32.5,
                radius: 30,
            };
        }else{
            circleVals = {
                center: 43,
                radius: 40,
            };
        }

        const style = {color: this.props.stroke};
        return(
            <div className="big-score-circle">
                <div className="big-score-circle__text">
                    <div style={style} className="big-score-circle__text__primary">
                        {this.props.score === null ? '-' : this.props.score}
                    </div>
                </div>
                <div className="big-score-circle__circle">
                    <svg>
                        { this.describeArc(circleVals.center, circleVals.center, circleVals.radius, 0, score, stroker) }
                    </svg>
                </div>
            </div>
        );
    }
}
// { this.describeArc(32.5, 32.5, 30, 0, score, stroker) }
