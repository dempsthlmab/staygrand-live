import React, { Component } from "react";



const Category = (props) => {
    if(props.category !== ''){
        return <p><span>{"Category: "}</span>{props.category}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Continent = (props) => {
    if(props.continent !== null){
        return <p><span>{"Continent: "}</span>{props.continent}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Country = (props) => {
    if(props.country !== null){
        return <p><span>{"Country: "}</span>{props.country}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Points = (props) => {
    if(props.points !== null){
        return <p><span>{"Points: "}</span>{props.points}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Placement = (props) => {
    if(props.placement !== null){
        return <p><span>{"Placement: "}</span>{props.placement}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Note = (props) => {
    if(props.note !== ''){
        return <p><span>{"Note: "}</span>{props.note}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};

export default class AwardItemTwo extends Component {
    addDomain(url) {
        if(url !== ''){
            if (!/^(f|ht)tps?:\/\//i.test(url)) {
                url = "https://www.staygrand.com" + url;
            }
        }
        return url;
    }
    imgExists(img){
        if(img !== ''){
            return (
                <img src={img} alt="" />
            );
        }
    }
    hasCategory(cat){
        if( cat !== ""){
            return (
                <p className="category"><span>Category: </span>{cat}</p>
            );
        }
    }
    render(){
        const Award = this.props;
        let image = this.addDomain(Award.image);
        return(
            <div className="award-item-wrapper-2">
                <div className="award-item-2">

                    <div className="award-item-body">
                        <div className="section-left">
                            <div className="award-item-icon">
                                { this.imgExists(image) }
                            </div>
                        </div>
                        <div className="section-right">
                            <p className="source">{Award.source}</p>
                            <p className="name">{Award.name} {Award.year}</p>
                            <Category category={Award.category} />
                            <Continent continent={Award.continent} />
                            <Country country={Award.country} />
                            <Points points={Award.points} />
                            <Placement placement={Award.placement} />
                            <Note note={Award.note} />
                        </div>
                    </div>
                    <div className="award-item-footer">
                        <div className="devider"></div>
                        <div className="award-item-type">
                            <p className="type">{Award.type}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
// <div className="award-item-header">
//     <p className="source">{Award.source}</p>
// </div>
// {this.props.reviewFor}
// { this.imgExists(image) }
