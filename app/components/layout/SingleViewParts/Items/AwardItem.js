import React, { Component } from "react";



const Category = (props) => {
    if(props.category !== ''){
        return <p><span>{"Category: "}</span>{props.category}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Continent = (props) => {
    if(props.continent !== null){
        return <p><span>{"Continent: "}</span>{props.continent}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Country = (props) => {
    if(props.country !== null){
        return <p><span>{"Country: "}</span>{props.country}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Points = (props) => {
    if(props.points !== null){
        return <p><span>{"Points: "}</span>{props.points}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Placement = (props) => {
    if(props.placement !== null){
        return <p><span>{"Placement: "}</span>{props.placement}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};
const Note = (props) => {
    if(props.note !== ''){
        return <p><span>{"Note: "}</span>{props.note}</p>;
    }else{
        return <p style={{display: 'none'}}></p>;
    }
};

export default class AwardItem extends Component {
    addDomain(url) {
        if(url !== ''){
            if (!/^(f|ht)tps?:\/\//i.test(url)) {
                url = "https://www.staygrand.com" + url;
            }
        }
        return url;
    }
    imgExists(img){
        if(img !== ''){
            return (
                <img src={img} alt="" />
            );
        }
    }
    hasCategory(cat){
        if( cat !== ""){
            return (
                <p className="category"><span>Category: </span>{cat}</p>
            );
        }
    }
    render(){
        const Award = this.props;
        let image = this.addDomain(Award.image);
        return(
            <div className="award-item-wrapper">
                <div className="award-item">
                    <div className="award-item-header">
                        { this.imgExists(image) }
                    </div>
                    <div className="award-item-body">
                        <p className="name">{Award.name} {Award.year}</p>
                        <p className="source"><span>Source: </span>{Award.source}</p>
                        <Category category={Award.category} />
                        <Continent continent={Award.continent} />
                        <Country country={Award.country} />
                        <Points points={Award.points} />
                        <Placement placement={Award.placement} />
                        <Note note={Award.note} />
                    </div>
                    <div className="devider"></div>
                    <div className="award-item-footer">
                        <p className="type">{Award.type}</p>
                    </div>
                </div>
            </div>
        );
    }
}
// {this.props.reviewFor}
// { this.imgExists(image) }
