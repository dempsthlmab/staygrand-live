import React, { Component } from "react";
import BigScoreCircle from "./Items/BigScoreCircle";

export default class RatingHeader extends Component {
    render(){
        return(
            <div className="review-section">
                <div className="containery">
                    <div className="review-section-header-wrapper">
                        <div className="review-section-header">
                            <div className="header-section">
                                <div className="section-1">
                                    <div className="score"><BigScoreCircle score={this.props.score} stroke={"#d1beab"} fill={"#f6f4f4"}/></div>
                                </div>
                                <div className="section-2">
                                    <div className="section-icon"></div>
                                    <div className="section-title">{"Review"}</div>
                                </div>
                            </div>
                            <div className="footer-section">
                                <div className="room-category">{"room type: "+this.props.roomCategory}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
// <BigScoreCircle score={review.get('avgRating')} fill={"#d1beab"}/>
// +this.props.roomCategory
//this.props.score
