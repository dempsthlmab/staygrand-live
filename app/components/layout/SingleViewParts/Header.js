import React, { Component } from "react";

import { Link } from 'react-router';

export default class Header extends Component {
    render() {
        const logo = '/assets/images/icons/sg-logo-white.svg';
        return (
            <div id="subpage-header">
                <Link className="small-logo" to="/">
                    <img src={logo} alt="Stay-geees" />
                </Link>
            </div>
        );
    }
}
