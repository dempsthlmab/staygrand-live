import React, { Component } from "react";
import CtfNoteItem from "./Items/CtfNoteItem";
import CtfNoNoteItem from "./Items/CtfNoNoteItem";
import CtfItem from "./Items/CtfItem";

export default class ClaimToFames extends Component {

    render(){
        const { claimtofames } = this.props;
        const claims = claimtofames.toJS();
        const CtfItems = claims.map((ctf, index) =>{
            return <CtfItem key={'1_'+index} note={ctf.note} type={ctf.claimToFameType.description}/>
        });
        return(
            <div className="claimtofame-section">
                { CtfItems }
            </div>
        );
    }
}
