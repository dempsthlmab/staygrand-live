import React, { Component } from "react";
import HotelMiniMap from './HotelMiniMap';

export default class HotelGeneralInfo extends Component {

    hasCollectionMember(){
        const { Hotel } = this.props;
        const collectionMember = Hotel.get('collectionMember');
        if(collectionMember !== null){
            return <li><span>{"Collection Member: "}</span>{collectionMember.getIn(['claimToFameType','description'])}</li>;
        }
    }

    render(){
        const { Hotel } = this.props;
        return(
            <div className="hotel-general-info-wrapper">
                <div className="single-info-map">
                    <HotelMiniMap Hotel={Hotel} />
                    <div onClick={this.props.loadMap} className="view-map-button">
                        <div><span></span></div> <span className="text">{"Open map view"}</span>
                    </div>
                </div>
                <div className="info">
                    <ul>
                        { this.hasCollectionMember() }
                        <li><span>{"Year Opened: "}</span>{Hotel.get('yearOpened')}</li>
                        <li><span>{"Hotel Chain: "}</span>{Hotel.get('chain')}</li>
                        <li><span>{"Owner: "}</span>{Hotel.get('owner')}</li>
                        <li><span>{"Rooms: "}</span>{Hotel.get('rooms')}</li>
                        <li className="homepage-link"><a href={Hotel.get("homepage")} target="_blank"><div><span></span></div> <span className="text">{"Go to the hotel website"}</span></a></li>
                    </ul>
                </div>
            </div>
        );
    }
}
