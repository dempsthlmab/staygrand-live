import React, { Component } from "react";
import AwardItem from "./Items/AwardItem";
import AwardItemTwo from "./Items/AwardItemTwo";
export default class HotelAwards extends Component {

    awardsRender(awards, awardItems){
        if(awards > 0){
            return(
                <div className="awards-section-body-wrapper">
                    <div className="containery">
                        <div className="awards-section-body">
                            {awardItems}
                        </div>
                    </div>
                </div>
            );
        }
    }

    render(){
        const hotelAwardsData = this.props.awards;
        const awardItems = hotelAwardsData.map((hotelAward) => {
            let category;
            const awardCat =  hotelAward.get('awardCategory');
            if(awardCat === null){
                category = '';
            }else{
                category = hotelAward.getIn(['awardCategory','name']);
            }
            return <AwardItemTwo key={hotelAward.get('awardId')+'_'+hotelAward.get('awardCategoryId')} name={hotelAward.getIn(['award','name'])} image={hotelAward.getIn(['award','imgSrc'])} type={hotelAward.getIn(['award','type'])} source={hotelAward.getIn(['award','source'])} category={category} year={hotelAward.getIn(['award','year'])} continent={hotelAward.get('awardContinent')} country={hotelAward.get('awardCountry')} placement={hotelAward.get('awardPlacement')} points={hotelAward.get('awardPoints')} note={hotelAward.get('awardNote')}/>
        });

        return(
            <div className="awards-section">
                <div className="containery">
                    <div className="awards-section-header-wrapper">
                        <div className="awards-section-header">
                            <div className="section-1">
                                <div className="score"><p>{this.props.numAwards}</p></div>
                            </div>
                            <div className="section-2">
                                <div className="section-icon"></div>
                                <div className="section-title">{"Hotel Awards"}</div>
                            </div>
                        </div>
                    </div>
                </div>
                { this.awardsRender(this.props.numAwards, awardItems) }
            </div>
        );
    }
}
