import React, { Component } from "react";
import RatingsCard from "./Items/RatingsCard";
import RatingsCardBlank from "./Items/RatingsCardBlank";


export default class RatingCards extends Component {

    render(){
        const { hotelRatings, ratingsInfo } = this.props;

        const valCategory = [
            { forVal: 0, category: "Not acceptable" },
            { forVal: 5, category: "Acceptable" },
            { forVal: 6, category: "Good" },
            { forVal: 7, category: "Really Good" },
            { forVal: 8, category: "Top Class" },
            { forVal: 9, category: "World Class" },
            { forVal: 10, category: "World Best" }
        ];

        const typesArray = [
            { main: "Location", number: "1-col"},
            { main: "Exterior", number: "2-col" },
            { main: "Interior/Design/Art", number: "3-col" },
            { main: "Service/Friendliness", number: "1-col" },
            { main: "Sense of Place/History", number: "2-col"  },
            { main: "Views", number: "3-col" },
            { main: "Dining", number: "1-col" },
            { main: "Bars", number: "2-col"  },
            { main: "Wellbeing", number: "3-col" },
            { main: "Fun/Events", number: "1-col" },
            { main: "Character/Charm", number: "2-col"  },
            { main: "Breakfast", number: "3-col" },
            { main: "Room", number: "1-col" },
            { main: "Bathroom", number: "2-col" },
            { main: "Bed", number: "3-col" },
            { main: "Minibar/Roomservice", number: "1-col"  },
        ];

        const infoArray = ratingsInfo.toJS();
        const cards = hotelRatings.map((rating, index) => {
            const ratingScore = rating.get('rating');
            const info = typesArray[rating.get('type') -1];
            const catVal = valCategory.find((cat) => {
                return cat.forVal === rating.get('rating');
            });
            if([10,9,8].indexOf(ratingScore) !== -1){
                return <RatingsCard key={index} data={rating} info={info} catVal={catVal.category}/>
            }else{
                return <RatingsCardBlank key={index} data={rating} info={info} catVal={catVal.category}/>
            }

        });

        return(
                <div className="rating-cards-wrapper">
                    <div className="containery">
                        <div className="rating-cards">
                            { cards }
                        </div>
                    </div>
                </div>
        );
    }
}
