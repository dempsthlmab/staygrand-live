import React, { Component } from "react";
import RatingCards from "./RatingCards";
import RatingHeader from "./RatingHeader";

export default class RatingsSection extends Component {
    render(){
        let { ratingsInfo, review, allAvgRating } = this.props;
        const hotelReview = review;
        const hotelRatings = hotelReview.get("ratings");
        const score = hotelReview.get('avgRating');
        const roomCategory = hotelReview.get('category');
        allAvgRating = allAvgRating.toJS();
        const graphScore = hotelRatings.toJS();
        const graphInfo = ratingsInfo.toJS();

        return(
            <div className="ratings-section">
                <RatingHeader score={score} roomCategory={roomCategory} />
                <RatingCards hotelRatings={hotelRatings} ratingsInfo={ratingsInfo} />
            </div>
        );
    }
}
