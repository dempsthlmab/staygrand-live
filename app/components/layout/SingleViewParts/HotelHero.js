import React, { Component } from "react";
import HotelHeroImages from "./HotelHeroImages";
import BigScoreCircle from "./Items/BigScoreCircle";

export default class HotelHero extends Component {
    render() {
        const Hotel = this.props.Hotel;
        const scoreImg = '/assets/images/icons/sg-logo-white.svg';
        const awardImg = '/assets/images/icons/sg-awards-white.svg';
        const opinionImg = '/assets/images/icons/sg-opinion-white.svg';
        let hasScoreDesc = false;
        if(Hotel.get('scoreDescription') !== null){
            hasScoreDesc = true;
        }
        return (
            <div id="hotel-hero">
                <div className="overlay-facts-parent">
                    <div className="overlay-facts-wrapper">
                        <div className="overlay-facts">
                            <div className="overlay-facts-stats">
                                <div className="item-scores">
                                    <div className="stats opinion">
                                        <div className="stat-number">{Hotel.get('numOpinions')}</div>
                                        <div className="stat-icon"><img className="opinion" src={opinionImg} /></div>
                                    </div>
                                    <div className="stats awards">
                                        <div className="stat-number">{Hotel.get('numAwards')}</div>
                                        <div className="stat-icon"><img className="awards" src={awardImg} /></div>
                                    </div>
                                    <div className="stats score">
                                        <div className={hasScoreDesc ? 'stat-number' : 'stat-number no-desc'}>{hasScoreDesc ? Hotel.get('scoreDescription') : '-'}</div>
                                        <div className="stat-icon"><img className="score" src={scoreImg} /></div>
                                    </div>

                                </div>
                            </div>
                            <div className="hotel-facts">
                                <h2 className="hotel-name">{Hotel.get('name')}</h2>
                                <span className="hotel-loaction">
                                    <span className="continent">{Hotel.get('continent')}, </span>
                                    <span className="country">{Hotel.get('country')}, </span>
                                    <span className="area">{Hotel.get('areaIsland') === "" ? "" : Hotel.get('areaIsland')+", "}</span>
                                    <span className="city">{Hotel.get('cityPlace')}</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <button className="gallary-button" onClick={(event) => { this.props.toggleGallery(); }}>{"View Images"}</button>
                </div>
                <HotelHeroImages Images={Hotel.get('images')} />
            </div>
        );
    }
}
// <BigScoreCircle score={Hotel.get('avgRating')} stroke={"#fff"} fill={"#6d7274"} windowW={this.props.windowW}/>
