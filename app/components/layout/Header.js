import React, { Component } from 'react';
import { Link } from 'react-router';
import Slider from 'react-slick';

export default class Header extends Component {
    render() {

      const settings = {
      fade: true,
      autoplay: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true
      };

      const HeroImage = "/assets/images/sg-hero-logo-black.png";
      const BetaImage = "/assets/images/sg-beta.png";
      // const imgUrl = "/assets/images";

      return (
        <div id="header-box" className="header-wrapper">
          <div className="header-logo">
            <Link to={'/'}>
                <img src={HeroImage} alt="" />
            </Link>
          </div>
          <div className="heroFadeShow">
            <Slider {...settings}>
              <div className="heroText">
                <p className="small">
                  The Worlds Most
                </p>
                <p className="large">
                  Praised Hotels
                </p>
              </div>
              <div className="heroText">
                <p className="small">
                The Finest Hotels
                </p>
                <p className="large">
                  on The Planet
                </p>
              </div>
              <div className="heroText">
                <p className="small">
                  The Most Awarded
                </p>
                <p className="large">
                  Hotels in The World
                </p>
              </div>
            </Slider>
            <div className="beta-icon">
              <img src={BetaImage} alt="" />
            </div>
          </div>
        </div>
      );
    }
}
