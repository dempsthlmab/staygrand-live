import React, { Component } from "react";

export default class Faq extends Component {
  render() {
    return (
        <div className="subpage-wrapper">
            <div className="containery">
                <h1>Faq - <span>Frequently Asked Questions</span></h1>
                <div className="grid">
                    <div className="grid-column single">
                        <h3>Who sets the hotel scores?</h3>
                        <hr className="underline" />
                        <div className="subpage-contant">
                            <p>
                                {"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus imperdiet cursus sollicitudin. In vel feugiat sem, at ultricies libero. Nam aliquet consectetur massa in luctus. Curabitur non ligula id libero tristique malesuada non sed mi. Quisque quis erat aliquet, vestibulum metus fermentum, ultricies nunc. Maecenas sit amet augue massa. Aliquam ut luctus diam. Vestibulum sit amet consectetur arcu, et tempor elit. Cras efficitur lacus at libero venenatis, eu tempus tortor laoreet. Vivamus consequat eget risus id condimentum. Praesent nunc lacus, tincidunt in ipsum sit amet, posuere egestas ipsum. Sed mollis risus et lectus rutrum condimentum. Donec porttitor dui eu dignissim aliquam."}
                            </p>
                        </div>
                    </div>
                    <div className="grid-column single">
                        <h3>Who sets the hotel scores?</h3>
                        <hr className="underline" />
                        <div className="subpage-contant">
                            <p>
                                {"Integer et libero feugiat, sagittis dolor eget, gravida massa. Duis malesuada auctor augue, placerat egestas urna fringilla quis. Donec egestas lacinia odio, at tempor orci. Donec ultrices lacus ligula, sit amet pretium nisi posuere in. Nulla facilisi. Phasellus mollis purus sit amet ante maximus, sit amet varius mauris molestie. Nunc vehicula sem sed dolor vestibulum, dapibus malesuada orci sagittis. Ut vitae diam nec quam congue convallis. Maecenas semper, nulla eu consequat pulvinar, augue lorem fermentum mi, in sodales erat lectus vitae dolor. Praesent facilisis purus vel ligula iaculis ultricies. Aliquam bibendum nulla ac justo volutpat, in varius odio finibus. Vestibulum accumsan lobortis sapien, eget commodo tellus malesuada quis. Ut tempor mauris ut mi fringilla lobortis."}
                            </p>
                            <p>
                                {"Phasellus in ornare nibh. Donec et tellus a risus aliquet tincidunt. Morbi dictum, lectus in mattis convallis, felis sapien eleifend ipsum, in elementum risus erat eu mi. Aliquam faucibus iaculis pulvinar. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis arcu eget leo cursus pellentesque ac non elit. Integer euismod risus quis elit tincidunt commodo. Maecenas non faucibus tellus."}
                            </p>
                        </div>
                    </div>
                    <div className="grid-column single">
                        <h3>Who sets the hotel scores?</h3>
                        <hr className="underline" />
                        <div className="subpage-contant">
                            <p>
                                {"Nunc a ligula justo. Integer at egestas arcu. Cras finibus tempus tellus in viverra. Nam massa lacus, eleifend eget ex non, pellentesque cursus velit. Cras enim justo, consequat ut est sed, dapibus pellentesque nisl. Suspendisse pellentesque ipsum vel ligula elementum ultrices. Aenean eget leo ac felis porttitor laoreet. Pellentesque aliquet aliquet tincidunt. Suspendisse nec pretium felis. Donec in egestas velit, a vehicula urna."}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}
