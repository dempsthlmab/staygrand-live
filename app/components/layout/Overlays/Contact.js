import React, { Component } from "react";

import ContactForm from '../ContactForm/ContactForm';
import ContactInfo from '../ContactForm/ContactInfo';

export default class Contact extends Component {
  render() {
    return (
      <div className="total-result-wrapper">
          <div className="containery-about">
            <div className="containery-border">
            <div className="grid-about grids-bottom">

                <h1>Reach out and say hello.</h1>
                <div className="">
                    <ContactInfo />
                </div>
            </div>
        </div>
        </div>
        </div>


    );
  }
}
