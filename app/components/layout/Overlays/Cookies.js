import React, { Component } from "react";

export default class Cookies extends Component {
  render() {
    return (
        <div className="total-result-wrapper">
            <div className="containery-about">
              <div className="containery-border">
              <div className="grid-about grids-bottom">
                <h1><center>Terms of use</center></h1>
                    <div className="subpage-contant">
                        <p>
                            This service (www.staygrand.com) is operated by StayGrand Nordic (“StayGrand”, “we”, “us” and “our”). Where we refer to “you” or “your”, this means or relates to you the user. These terms of use, alongside our privacy policy, govern your use of all Stay Grand services.
                        </p>

                        <p className="small-title">
                            Changes to the agreement
                        </p>

                        <p>
                            Stay Grand may, from time-to-time and at its own discretion, change these terms of use. Any such changes will become effective immediately when posted on this website. Your continued use of this service will constitute your on-going acceptance of any changes to the terms of use. Please check this page regularly for updates.
                        </p>

                        <p className="small-title">
                            Usage of the services and trademarks
                        </p>

                        <p>
                            Stay Grand follows the fair use doctrine of copyright law. By using this service, you agree that you will not do any of the following:
                        </p>

                        <p className="other-title">
                                A.  Cache, license, transfer, or sell large portions of the information found on this service, beyond the fair use doctrine of copyright law.
                        </p>

                        <p className="other-title">
                                B.  Scrape, crawl, index, spider any of the content available on or through the service by any automated process or otherwise.
                        </p>

                        <p className="other-title">
                                C.  Use any Stay Grand trademark, or copy and imitate our design, look and feel, on any websites, services, products or companies.
                        </p>

                        <p className="other-title">
                                D.  Imply that we endorse your products, services or company without written permission from us.
                        </p>


                        <p>
                            Further, you agree to not use the service in any way that breaches applicable local, national, or international law. You also agree to not disrupt, modify, damage or interfere with the service or its associated software, hardware and servers in any way.
                        </p>

                        <p className="small-title">
                            Copyright and third-party websites
                        </p>

                        <p>
                            Should you wish to use or adapt any of the content found on this service, please follow and refer to the fair use doctrine of copyright law. Similarly, this service includes quotations, opinions and small portions of content from other third-party services, either by agreement with those services or through reliance on the fair use doctrine of copyright law.

                            This service includes links to websites and other services which are operated and owned by third parties. These links do not constitute an endorsement by Stay Grand of those third-party services. We do not assume any responsibility for the content, policies or practices of such websites or third parties.

                            You may link to our service provided you do so in a way that is fair and legal, and does not damage or take advantage of our reputation.

                        </p>

                        <p className="small-title">
                            Intellectual property
                        </p>

                        <p>
                            All content and other material found on this service is owned by us or third-party sources. You acknowledge and agree that Stay Grand and/or these third-party sources retain all rights, title, ownership and interests, including all data, information, content and materials provided on or through the service. This includes, without limitation, text, software, photographs, videos, graphics, scripts, headers, sounds, the selection and arrangements of elements displayed on our through the service, the compilation of all content and materials, and the business process, procedures, methods and techniques.

                            All third-party trademarks, content, logos, photographs and other intellectual property contained on or through this service are the property of respective third parties and are protected by applicable copyright, trademark or other intellectual laws.
                            Stay Grand’s intellectual property, including all content and proprietary trademarks, trade names, slogans and logos, may not be used in any manner that is likely to cause confusion, or in any manner that disparages Stay Grand.

                        </p>

                        <p className="small-title">
                            Security and privacy
                        </p>

                        <p>
                            The information that you provide about yourself to us or otherwise collected by us will only be used in accordance with our privacy policy.
                        </p>


                        <p className="small-title">
                            Limitation of liability
                        </p>

                        <p>
                            Stay Grand provides access to the service on an “as is” and “as available” basis, without warranty of any kind. To the maximum extent permitted by applicable law, Stay Grand does not accept any liability for direct, indirect, incidental, consequential, exemplary, punitive or other damages or losses of any kind arising from your use of this service.

                            We will not be held liable for any loss or damage caused by viruses or other technologically harmful material that may be transmitted via the service. We do not guarantee that that access to the service will be uninterrupted or that you will be able to use the service at times or locations of your choosing. Further, we do not guarantee that there will be no errors or out-of-date information in our content, and we disclaim all liability for any inaccuracies.

                            We do not endorse, warrant or guarantee any third-party product or service offered through the service and will not be in any way be responsible for monitoring any transaction between you and third-party providers of products or services.

                        </p>

                        <p className="small-title">
                            Complaints
                        </p>

                        <p>
                            We respect the intellectual property and rights of others. If you are a copyright owner or you find that your work has been copied or is accessible on the service in a way that constitutes copyright infringement, please contact us.
                        </p>


                        <p className="small-title">
                          General
                        </p>

                        <p>
                            This agreement constitutes the entire agreement between you and us, and replaces all prior understandings, written or oral. If any terms of this agreement are found to be invalid or unenforceable, the remainder of these terms of use will remain valid and enforceable.
                            This agreement will be enforced and governed by the laws of Sweden, and will be subjected to the jurisdiction of the courts of Sweden.
                            If you have any questions regarding these terms of use, please feel free to contact us.
                        </p>

                        <p className="small-title">
                            Privacy policy
                        </p>

                        <p>
                            This privacy policy covers this website (www.staygrand.com) and all services produced by Stay Grand Nordics (“Stay Grand”, “we”, “us” and “our”). By using any service provided by Stay Grand, you are agreeing and consenting to the practices described in this privacy policy, as well as our terms of use.

                            This privacy policy will inform you of Stay Grand’s policy and practices regarding the collection, use and disclosure of any information that may be directly or indirectly related to you, which you submit or disclose when accessing this service.

                            We are committed to safeguarding your personal information. However, if you do not agree to all of this privacy policy, please do not access the service.

                        </p>

                        <p className="small-title">
                            Changes to this policy
                        </p>

                        <p>
                            Stay Grand may, from time-to-time and at its own discretion, change these terms of use. Any such changes will become effective immediately when posted on this website. Your continued use of this service will constitute your on-going acceptance of any changes to the privacy policy. Please check this page regularly for updates.
                        </p>

                        <p className="small-title">
                          The information we collect
                        </p>

                        <p>
                            We may receive personal data about you from other sources and third parties that help us improve your Stay Grand experience. As you use our service, certain information may also be passively collected and stored, including your IP address, browser type and operating system. We may also use cookies (see section 5) and navigational data like Uniform Resource Locators (”URLs”) to gather information regarding the date and time of your visit, which pages you visit and how long you spend on each of them.
                            This type of information is collected to make Stay Grand more useful to you and to meet your special interests and needs.
                            The service has technical measures in place to protect against unauthorised or unlawful use of your personal information. However, no data transmission over the internet is completely secure and we cannot guarantee the security of any information you transmit to us.
                        </p>

                        <p className="small-title">
                            How we use your information
                        </p>

                        <p>
                            Any data that is submitted to us is used to provide you with access to the service, improve your experience and better tailor the features.
                            We may use the information to customise your experience by offering you additional, tailored information, advertisements, promotions and other functionalities. We may process the personal information to communicate with you about certain aspects of the service.
                            Stay Grand may provide your personal data to select third party service providers. These third party sources may only use the data in accordance to our instructions.
                            We may promote third party products and services by placing advertisements on the service on behalf of partners and ad servers. These partners may use cookies or other means to monitor your interaction with the advertisements. Clicking on such advertisements may direct you to a partner website - this privacy policy does not cover the privacy practices of any of our partners.
                        </p>

                        <p className="small-title">
                            How your information is shared
                        </p>

                        <p>
                            We do not rent, sell, or share your information with other people or nonafﬁliated companies for their direct marketing purposes. We may share your data with approved third party providers who work on our behalf, or with us, to provide you with services - but only when this information is needed for the functionality of these services. When you use this service, you are consenting to the disclosure of your personal information to our partners.
                            Stay Grand may use your information for processing, managing and providing you with retail offers, advertisements and other information related to your profile and interests. By using this service, you agree that we may contact you to conduct targeted advertising based on the information provided by you and collected by us.
                        </p>

                        <p className="small-title">We may also share your information if:</p>
                             <p className="other-title">- we sell or buy any business or assets, where your personal information may be disclosed to the prospective seller or buyer</p>
                             <p className="other-title">- our assets are acquired by a third party.</p>


                        <p className="small-title">
                            Cookies
                        </p>

                        <p>
                            Stay Grand may use cookies to enhance your user experience. Cookies are pieces of information that a website transfers to your hard drive to store and sometimes track information about you. This is a common practice and they are used to simply store and gather information. We may collect information about your computer including your IP address, operating system and browser type.
                            You are not obliged to accept cookies and you can change your browser’s settings to stop cookies from being accepted on your computer. If you do so, however, you may not be able to take full advantage of this service.
                        </p>

                        <p className="small-title">
                            Accessing or reviewing your personal information
                        </p>

                        <p>
                            You can request to see the information we have gathered on you at any time. If you wish to access the personal information, please contact us via the link below.
                            If we are legally able to, we will be provide you with copies of all the information we have collection on you. We will request that you provide us with identification so that we may be certain that you are entitled to receive the requested data.
                        </p>

                        <p className="small-title">
                            Questions
                        </p>

                        <p>
                            If you have any questions about this privacy policy, please contact us.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    );
  }
}
