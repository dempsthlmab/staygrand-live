import React, { Component } from "react";

export default class About extends Component {
  render() {
    return (
        <div className="total-result-wrapper">
          <div className="containery-about">
            <div className="containery-border">
            <div className="grid-about">
              <h1><center>About & FAQ</center></h1>
                <p className="small-title underline">WHAT IS STAY GRAND?</p>
                <div className="subpage-contant">
                <p>
                  Stay Grand is the ultimate way to find your stay. We offer professional, unbiased and curated hotel recommendations all over the world. Whether you are looking
                  for the hippest hotel, the respected classic or the outstanding resort, we know where you should stay.
                </p>

                <p className="small-title">
                    HOW DO WE KNOW?
                </p>

                <p>
                  We trust the best. Our list of hotels is based on awards given by the world’s most respected travel media. If it’s awarded by someone we trust, it’s worth our attention. And yours.</p>

                <p className="small-title">
                    WE CHECK IN TO CHECK IT OUT
                </p>

                <p>
                  Our team of professional reviewers do regular check-ups of selected hotels. We categorise the hotels and highlight key features so you can find the right hotel for you, whether your preference is a world-class spa, a buzzing pool scene or a hedonistic nightspot.</p>

                <p className="small-title">
                    ALWAYS UP-TO-DATE
                </p>

                <p>
                  The editorial team at Stay Grand keeps constant track of the news in the hotel business. We analyse and publish opinions and news that might influence our recommendations - and your choice. We’ve done the hard work for you - because, after all, everyone deserves to stay grand.
                </p>
              </div>
              <div className="">
                <p className="small-title underline">FAQ</p>

                <div className="subpage-contant">

                  <p className="small-title">
                      How do you decide which hotels to include?
                 </p>

                 <p>
                     We add a hotel to our portfolio when it has received an award from a respected source; when it has been endorsed by multiple professionals;
                     or when our own expert reviewers have stayed there and want to share the experience with you. This means that our hotel collection is highly
                     selective. The hotels can be fabulous overall or excel within a certain area (creative cuisine, cool pool, chic design - you get the idea).
                     No hotel can pay to be featured in Stay Grand.
                 </p>

                 <p className="small-title">
                     How do you know which awards and sources are best?
                 </p>

                 <p>
                     We know that you’re oversaturated with information and do not have the time to trawl through pages and pages of opinions,
                     trying to figure out who and what to trust. We have expertise in this field, and we know we can trust fellow professionals - the likes of Condé Nast Traveler,
                     Forbes, The Telegraph, etc. We’ve filtered out the noise and only include the awards, advice and opinions from the world’s most respected travel media.
                 </p>

                 <p className="small-title">
                  But who are Stay Grands own reviewers?
                 </p>

                 <p>
                     Our team of travel specialists have decades of experience scouring the globe for those gems which are so hard to find. We love what we do, have a passion for staying grand, and get a real buzz from sharing with you the finer details of a stay at a memorable hotel.
                 </p>

                 <p className="small-title">
                     Can you explain Stay Grand’s own award system?
                 </p>

                 <p>
                     Our editorial team stays anonymously at hotels and scrutinizes every aspect of them (right down to the thread count of the sheets). Once a hotel meets our strict criteria, they may deserve the following medals for excellence in specific areas and categories (wellness, interiors, bed, and so on):
                 </p>

                 <p className="other-title">
                     GOLD: World Best (“Truly exceptional”)
                 </p>
                 <p className="other-title">
                     SILVER: World Class (“Outstanding”)
                 </p>
                 <p className="other-title">
                     BRONZE: Top Class (“Demonstrating excellence”)
                 </p>

                 <p>
                     The more medals a hotel has, the greater an all-rounder it is.
                 </p>

                 <p className="small-title">
                     How do you keep your content up-to-date?
                 </p>

                 <p>
                     Our team of experts have their fingers firmly on the hotel world’s pulse. We have a system for constantly scanning the global media for the latest reviews and news on our hotels, and this system notifies us whenever a new award is released. Plus our editorial team is frequently checking in and checking out the hotels in our portfolio. That way, you’ll have all the latest and most valuable advice and recommendations at your fingertips.
                 </p>
                </div>
              </div>
              <div className="">
                <div className="subpage-contant">
                <p className="small-title underline">Media Source List 2017</p>
                  <p>
                    Stay Grand includes quotations and editorial opinions from only the most respected and renowned publications across the globe. On occasion our team may include opinions, reviews and editorial quotes from sources beyond this list - but only when the publication is proven to be reputable.
                  </p>
                <p className="sources">Sources:</p>
                <p className="source-margin">
                  <p>
                    <a className="source-title" href="http://www.robbreport.com">Robb Report</a>
                  </p>
                  <p className="italic">
                    Available in 17 international editions, the self-proclaimed “Manual of Modern Luxury” focuses affluence, true luxury, and the very best experiences.
                  </p>
                </p>
                <p className="source-margin">
                  <p>
                    <a className="source-title" href="http://www.elitetraveler.com">Elite traveler</a>
                  </p>
                  <p className="italic">
                    A private jet lifestyle magazine guiding high net worth individuals to the best hotels, restaurants, travel and more.
                  </p>
                </p>
                <p className="source-margin">
                  <p>
                    <a className="source-title" href="http://www.departures.com">Departures</a>
                  </p>
                  <p className="italic">
                    An American quarterly lifestyle magazine published by Time Inc. that covers the world’s best high-end hotels, travel and lifestyle, as well as the hottest destinations.
                  </p>
                </p>
                <p className="source-margin">
                  <p>
                    <a className="source-title" href="http://www.businesstraveller.com">Business Traveller</a>
                  </p>
                  <p className="italic">
                    With 14 editions available, this magazine informs frequent business travellers on the latest news about hotels, airlines, and destinations.
                  </p>
                </p>
                <p className="source-margin">
                  <p>
                    <a className="source-title" href="http://www.cntraveller.com" alt="(US, UK, select international editions)">Condé Nast Traveler / Condé Nast Traveller</a>
                  </p>
                  <p className="italic">
                    Award-winning luxury lifestyle publications sharing travel inspiration and advice with its audience of discerning “global citizens”.
                  </p>
                </p>
                <p className="source-margin">
                  <p>
                    <a className="source-title" href="http://www.travelandleisure.com">Travel + Leisure</a>
                  </p>
                  <p className="italic">
                    A highly popular monthly travel magazine with almost five million readers. It targets the sophisticated traveller with inspirational editorial on the most exciting destinations.
                  </p>
                </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.wallpaper.com">Wallpaper*</a>
                    </p>
                    <p className="italic">
                      An edgy, urban and influential design magazine focusing architecture, fashion, travel, art and lifestyle.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.frommers.com/">Frommers</a>
                    </p>
                    <p className="italic">
                      A well-known and expansive guidebook series (featuring more than 350 guidebooks) as well as a popular website sharing candid travel advice and editorial.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.fodors.com/">Fodors</a>
                    </p>
                    <p className="italic">
                      A pioneering guidebook which has become the world’s largest publisher of travel information, with more than 440 guides, a popular website and 700 researchers placed all over the world.
                    </p>
                 </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.suitcasemag.com">Suitcase</a>
                    </p>
                    <p className="italic">
                      This award-winning, innovative quarterly magazine features holiday destinations by theme, and is targeted towards the modern traveller.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.natgeotraveller.co.uk">National Geographic Traveller</a>
                    </p>
                    <p className="italic">
                      One of the world’s leading travel magazines and a household name, this magazine focuses on stunning photography and storytelling.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.vogue.com/living/travel">Vogue (UK/US/select international editions)</a>
                    </p>
                    <p className="italic">
                      A prestigious and highly influential fashion and lifestyle publication, available worldwide in multiple editions.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.tatler.com/travel">Tatler</a>
                    </p>
                    <p className="italic">
                      This high-society British magazine is over a century old and shares all the finest things in luxury travel, fashion and lifestyle.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.forbes.com/sites/forbestravelguide/#20ada92c2522">Forbes</a>
                    </p>
                    <p className="italic">
                      A leading American business magazine and website, providing a respected star rating service for hotels via Forbes Travel Guide.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.luxurytravelmag.com.au/">Luxury Travel Magazine</a>
                    </p>
                    <p className="italic">
                      An Australian magazine dedicated to sharing the world’s finest hotels and travel experiences.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.monocle.com">Monocle</a>
                    </p>
                    <p className="italic">
                      Published ten times per year, this magazine shares the latest in global affairs, lifestyle and design for a globally-minded audience. They also have a series of chic travel guides.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.lonelyplanet.com/magazine">Lonely Planet Traveller</a>
                    </p>
                    <p className="italic">
                      The largest and best-known travel guidebook publisher in the world. The award-winning monthly magazine focuses on inspirational travel.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.thesundaytimes.co.uk/sto/public/travel_magazine">Sunday Times Travel Magazine</a>
                    </p>
                    <p className="italic">
                      Available almost exclusively in print, this is a monthly travel magazine from the UK’s largest-selling “quality press” newspaper.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.gq-magazine.co.uk/topic/travel">GQ (select international editions)</a>
                    </p>
                    <p className="italic">
                      An international monthly men’s magazine focusing on style and fashion, while also featuring travel guides on the best places to go to.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.theguardian.com/travel">The Guardian</a>
                    </p>
                    <p className="italic">
                      A prestigious British daily newspaper which shares the latest in travel news and reviews, as well as travel guides.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.nytimes.com/section/travel?action=click&pgtype=Homepage&region=TopBar&module=HPMiniNav&contentCollection=Travel&WT.nav=page">New York Times</a>
                    </p>
                    <p className="italic">
                      A multiple award-winning American daily newspaper, with a popular online and print travel section.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.telegraph.co.uk/travel">The Telegraph</a>
                  </p>
                  <p className="italic">
                      A quality UK broadsheet newspaper with a large and highly respected travel segment.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.howtospendit.ft.com/travel">Financial Times / How to Spend it - Financial Times</a>
                    </p>
                    <p className="italic">
                      An award-winning luxury lifestyle magazine from the world’s leading business newspaper, the Financial Times.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://eu.louisvuitton.com/eng-e1/stories/city-guides-2016#/cityguides">Louis Vuitton City Guides</a>
                    </p>
                    <p className="italic">
                      Luxury lifestyle travel guides from the high-end fashion house, Louis Vuitton.
                    </p>
                  </p>
                  <p className="source-margin">
                    <p>
                      <a className="source-title" href="http://www.foodandtravel.com">Food & Travel</a>
                    </p>
                    <p className="italic">
                      Award-winning Food and Travel magazine combines your love of adventure, gastronomy and stunning photography.
                    </p>
                  </p>
                </div>
              </div>
              <div className="grid">
                <p className="small-title underline">Awards List 2017</p>
                  <div className="subpage-contant">
                    <p>
                    Stay Grand’s portfolio of hotels is largely based on awards given by the world’s most respected sources - from industry organisations to travel publications. These awards are the most legitimate and prestigious prizes in the hotel and travel industry.
                    </p>
                    <br />
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">Condé Nast Traveler (US)</span>
                      </p>
                      <p className="awards-text-block">
                        <p>
                          Gold List
                        </p>
                        <p>
                          <span className="italic">Condé Nast Traveler's annual collection of their editors' absolute favourite hotels across the world.</span>
                        </p>
                      </p>
                      <p className="awards-text-block">
                           <p>
                            Hot List
                          </p>
                         <p>
                           <span className="italic">The editorial team have vetted hundreds of hotels across the globe to draw up this selection of what they think are the year’s top hotel openings.</span>
                         </p>
                      </p>
                      <p className="awards-text-block">
                         <p>
                            Readers' Choice Awards
                         </p>
                         <p>
                           <span className="italic">Annual survey of more than 300,000 readers, resulting in this winning list of favourites.</span>
                         </p>
                       </p>
                      </p>
                      <p className="awards-margin">
                        <p>
                          <span className="source-title">Condé Nast Traveller (UK)</span>
                        </p>
                        <p className="awards-text-block">
                          <p>
                              Gold List
                          </p>
                        <p>
                          <span className="italic">A yearly list of favourites from the editors of Condé Nast Traveller.</span>
                        </p>
                        </p>
                          <p className="awards-text-block">
                            <p>
                              Hot List
                            </p>
                            <p>
                              <span className="italic">The editors’ annual selection of the best new hotels in the world.</span>
                            </p>
                          </p>
                          <p className="awards-text-block">
                            <p>
                              The 8 Best Spas in the World
                            </p>
                            <p>
                              <span className="italic">The editors’ pick of the world's most re-energising destination spas.</span>
                            </p>
                          </p>
                      </p>
                      <p className="awards-margin">
                        <p>
                          <span className="source-title">Travel + Leisure</span>
                        </p>
                        <p className="awards-text-block">
                          <p>
                          It List
                         </p>
                          <p>
                            <span className="italic">The editors’ annual collection of the best new - or radically redone - hotels around the world.</span>
                          </p>
                        </p>
                        <p className="awards-text-block">
                          <p>
                            World's Best Awards
                         </p>
                          <p>
                            <span className="italic">Yearly award polled from the readers of Travel + Leisure ranking the 100 best hotels in the world.</span>
                          </p>
                        </p>
                      </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">Sunday Times Magazine</span>, The Best in The World
                        </p>
                      <p>
                          <span className="italic">A yearly list of the editors’ favourite hotels around the world, presented via a number of different categories.</span>
                        </p>
                     </p>
                      <p className="awards-margin">
                        <p>
                          <span className="source-title">Tatler</span>, 101 Best Hotels in The World
                        </p>
                        <p>
                          <span className="italic">The editors at Tatler Travel Guide have selected their favourite hotels around the world based on first-hand experience of each property.</span>
                        </p>
                      </p>
                    <p className="awards-margin">
                        <p>
                          <span className="source-title">The Telegraph</span>, The 50 Greatest Hotels in The World
                        </p>
                        <p>
                          <span className="italic">A collection of the greatest hotels according to the travel and hotel experts at The Telegraph. Places included "offer something extraordinary" and will "affect the way you feel".</span>
                        </p>
                      </p>
                    <p className="awards-margin">
                        <p>
                          <span className="source-title">Andrew Harper</span>, Grand Awards
                        </p>
                        <p>
                          <span className="italic">A carefully curated selection of the most  memorable hotels and resorts of the previous 12 months, as chosen by Andrew Harper's travel experts.</span>
                        </p>
                      </p>
                    <p className="awards-margin">
                        <p>
                          <span className="source-title">Elite Traveler</span>, Top 100 Suites
                        </p>
                        <p>
                          <span className="italic">Elite Travelers’ list of Top 100 Suites picks out the world’s most iconic rooms to stay in each year, recognising where hotels have created something extraordinary.</span>
                        </p>
                      </p>
                      <p className="awards-margin">
                        <p>
                          <span className="source-title">Forbes Travel Guide</span>, Star Awards
                        </p>
                        <p>
                          <span className="italic">Forbes' team of incognito inspectors have travelled the world regularly assessing properties in order to draw up this yearly list of Global Star Ratings, which has become a benchmark for luxury travel. We have included the Forbes Five Star and Four Star hotels.</span>
                        </p>
                      </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">World Travel Awards</span>
                      </p>
                      <p>
                        <span className="italic">Dubbed the "Oscars" of the travel industry, this award celebrates excellence across a range of categories. We have included the hotels and resorts winning at a> regional level (i.e South America, Middle East, Europe).</span>
                      </p>
                    </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">Boutique Hotel Awards</span>, World Boutique Hotel Awards
                      </p>
                      <p>
                        <span className="italic">This international awards organisation is dedicated to recognising excellence among luxury boutique hotels, championing those that demonstrate unique character whilst providing world-class services.</span>
                      </p>
                    </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">European Hotel Design Awards</span>, European Hotel Design Award Winners
                      </p>
                      <p>
                        <span className="italic">The award celebrates exceptional hotel design and architecture, with the winners selected by a jury of top hospitality executives and design experts.</span>
                      </p>
                    </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">Monocle</span>, The Restaurant Awards
                      </p>
                      <p>
                        <span className="italic">Monocle's annual restaurant award commends places that have been consistently good and the up-and-comers that promise to be great restaurants in decades to come. We have included all restaurants within or affiliated to hotels.</span>
                      </p>
                    </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">Wallpaper*</span>, Design Awards
                      </p>
                      <p>
                        <span className="italic">Wallpaper*'s jet-set panel of judges have chosen the most unique and inspiring city hotels around the world. We have included the winning as well as shortlisted properties.</span>
                      </p>
                    </p>
                    <p className="awards-margin">
                     <p>
                        <span className="source-title">Michelin Guide</span>, Michelin Star Awards
                      </p>
                     <p>
                        <span className="italic">The Michelin Guide’s renowned annual restauranspan className="source-title" star rating award. We have included the hotel restaurants which currently hold Michelin starsspan</span>
                      </p>
                   </p>
                    <p className="awards-margin">
                     <p>
                        <span className="source-title">Luxury Travel Magazine</span>, Gold List
                     </p>
                     <p>
                       <span className="italic">This award recognises the ultimate in luxury travel in Australia and around the world,  as voted for by the magazine's readers and an expert editorial panel. We have included both the winners and the finalists of selective categories.</span>
                     </p>
                   </p>
                    <p className="awards-margin">
                      <p>
                        <span className="source-title">Condé Nast Johansens</span>, Awards for Excellence
                      </p>
                      <p>
                        <span className="italic">Readers vote for their favourite properties within Condé Nast Johansens' collection of recommended properties, all of which have been annually selected by their team of experts. This award recognises the "very best of the best".</span>
                      </p>
                   </p>
                  </div>
              </div>
            </div>
            </div>
          </div>
        </div>
    )
  }
}
