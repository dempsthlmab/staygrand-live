import React, { Component } from "react";

export default class ContactInfo extends Component {
  render() {
    return (

            <div className="contact-info">

                    <section className="email">
                        <p className="small-title">GENERAL</p>
                        <hr className="underline"/>
                        <p><a href="mailto:hello@staygrand.com">{"hello@staygrand.com"}</a></p>
                    </section>

                    <section className="email">
                        <p className="small-title">ADS & PARTNERSHIPS</p>
                        <hr className="underline"/>
                        <p><a href="mailto:advertise@staygrand.com">{"advertise@staygrand.com"}</a></p>
                    </section>

                    <section className="email">
                        <p className="small-title">PRESS & MEDIA</p>
                        <hr className="underline"/>
                        <p><a href="mailto:press@staygrand.com">{"press@staygrand.com"}</a></p>
                    </section>

                    <section className="email">
                        <p className="small-title">EDITORIAL</p>
                        <hr className="underline"/>
                        <p><a href="mailto:editor@staygrand.com">{"editor@staygrand.com"}</a></p>
                    </section>

                    <section className="email">
                        <p className="small-title">JOBS</p>
                        <hr className="underline"/>
                        <p><a href="mailto:hello@staygrand.com">{"hello@staygrand.com"}</a></p>
                    </section>


        </div>
    );
  }
}
