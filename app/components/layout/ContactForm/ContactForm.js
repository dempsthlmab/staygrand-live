import React, { Component } from "react";

export default class ContactForm extends Component {
    constructor(){
        super();
            this.state = {
                name: '',
                email: '',
                message: '',
            }

    }
  render() {
    return (
        <div className="grid-column dubble">
            <div className="contact-form">
                <h3>Drop us a line!</h3>
                <form onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" name="from" placeholder="Fullname" onChange={(e) => this.handleOnChange(e, 'name')}/>
                    <input type="text" name="email" placeholder="Email" onChange={(e) => this.handleOnChange(e, 'email')}/>
                    <textarea name="message" rows="8" cols="80" placeholder="Message" onChange={(e) => this.handleOnChange(e, 'message')}></textarea>
                    <input type="submit" value="Send Message"/>
                </form>
            </div>
        </div>
    );
  }
  handleSubmit(val){
      val.preventDefault();
      const email = this.state.email;
      if(this.validateEmail(email)){
        //   console.log('Email is solid!');
      }else{
        //   console.log('Email is up to someting');
      }
  }
  handleOnChange(e, type){
      if(type === 'name'){
          this.setState({ name: e.target.value})
      }else if(type === 'email'){
          this.setState({ email: e.target.value})
      }else if(type === 'message'){
          this.setState({ message: e.target.value})
      }
  }
  validateEmail(email){
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
  }
}
