import React, { Component } from 'react'
import { Link } from 'react-router';

import About from "./Overlays/About";
import Faq from "./Overlays/Faq";
import Cookies from "./Overlays/Cookies";
import Contact from "./Overlays/Contact";

class Footer extends Component {

    //State values for showing the footer overlay and which component to render
    constructor() {
        super();
        this.state = {
            overlayActive: false,
            activeOverlay: null,
        }
    }
    /*
    Checks witch component should be rendered and renders that
    */
    selectOverlay(overlayActive, activeOverlay){
        if(overlayActive){
            if(activeOverlay === 'about'){
                return(
                    <About />
                );
            }else if(activeOverlay === 'faq'){
                return(
                    <Faq />
                );
            }else if(activeOverlay === 'cookies'){
                return(
                    <Cookies />
                );
            }else if(activeOverlay === 'contact'){
                return(
                    <Contact />
                );
            }
        }
    }

    /*
        Checks if it is the start page or singlehotel page.
        If the user is on the singlehotel page a link tag will be wrapped around the staygrand logo that sends the user
        to the home page.
    */
    renderLinkOrNot(path, imageSrc){
        if(path !== '/'){
            return(
                <li><Link className="navlink" to={"/"}><img src={imageSrc} /></Link></li>
            );
        }else{
            return(
                <li><img src={imageSrc} /></li>
            );
        }
    }
    /*
        This function looks the scroll of the page so that you cannot scroll while the overlay is open.
    */
    lockScroll(lock){
        if(lock){
            document.body.style.overflow = 'hidden';
        }else{
            document.body.style.overflow = 'auto';
        }
    }
  render() {
      const sgLogo = "/assets/images/sg-white-symbol.png";
    return (
        <div id="footer-box" className="footer">
            <div className="containery">
                <ul>
                    <li><a className="navlink" href="#" onClick={this.linkOnClick.bind(this, 'about')}>About & FAQ</a></li>
                    <li><a className="navlink" href="#" onClick={this.linkOnClick.bind(this, 'cookies')}>Terms Of Use</a></li>
                    {this.renderLinkOrNot(this.props.location, sgLogo)}
                    <li><a className="navlink" href="https://www.instagram.com/staygrand_hotels" target="_blank">Instagram</a></li>

                    <li><a className="navlink" href="#" onClick={this.linkOnClick.bind(this, 'contact')}>Contact</a></li>
                </ul>
            </div>
            <div className={this.state.overlayActive ? "overlay-wrapper active" : "overlay-wrapper"}>
                <div className="close-overlay" onClick={(event) => { this.closeOverlay()}}><i></i><i></i></div>
                { this.selectOverlay(this.state.overlayActive, this.state.activeOverlay) }
            </div>
        </div>
    );
  }
  linkOnClick(overlay, e){
      e.preventDefault();
      if(this.state.activeOverlay === overlay){
          return false;
      }else{
          this.setState({overlayActive: true, activeOverlay: overlay});
          this.lockScroll(true);
      }
  }
  closeOverlay(){
      this.setState({overlayActive: false, activeOverlay: null});
      this.lockScroll(false);
  }
}
function mapStateToProps() {
  return {}
}

export default Footer
