/*
This is the loading spinner component for when a user makes a filtering or a search.
*/
import React, { Component } from "react";

export default class LoadingNew extends Component {
  render() {
    return (
        <div className="spin-container">
            <div className="loading-wrapper">
                <div className="spinner2"></div>
            </div>
        </div>
    );
  }
}
