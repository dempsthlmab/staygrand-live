import React, { Component } from "react";

export default class ScrollTopBtn extends Component {

    constructor() {
      super();

      this.state = {
          intervalId: 0,
          show: false,
          shouldStick: false
      };
      this.onScroll = this.scrollCheck.bind(this);
    }

    componentDidMount(){
        window.addEventListener('scroll', this.onScroll, false);
    }
    componentWillUnmount(){
        window.removeEventListener('scroll', this.onScroll, false)
    }

    scrollCheck(){
        const windowH = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;
        const headerH = document.getElementById('header-box').clientHeight;
        const footerH = document.getElementById('footer-box').clientHeight;
        const bodyH = document.body.clientHeight;
        const shouldStop = bodyH - (windowH + footerH);

        if(window.scrollY >= headerH){
            if(!this.state.show){
                this.setState({ show: true });
            }
            if(window.scrollY >= shouldStop){
                this.setState({ shouldStick: true })
            }else if(window.scrollY < shouldStop){
                this.setState({ shouldStick: false })
            }
        }else if(window.scrollY < headerH){
            this.setState({ show: false });
        }
    }

    scrollStep() {
        const scrollStepInPx = 50;
        if (window.pageYOffset === 0) {
            clearInterval(this.state.intervalId);
        }
        window.scroll(0, window.pageYOffset - scrollStepInPx);
    }

    scrollToTop() {
        const delayInMs = 0.5;
        let intervalId = setInterval(this.scrollStep.bind(this), delayInMs);
        this.setState({ intervalId: intervalId });
    }

    render() {
        let activeClass;

        if(this.state.show && this.state.shouldStick){
            activeClass = "scroll-top-btn show stick";
        }else if(this.state.show){
            activeClass = "scroll-top-btn show";
        }else{
            activeClass = "scroll-top-btn";
        }

        return (
            <button id="scroll-top" className={activeClass} onClick={ () => { this.scrollToTop(); }}>

            <img src="/assets/images/icons/to-top.png"/>
            </button>
        );
    }
}
