import React, { Component } from "react";
import { Link } from 'react-router';
import ShortOpinions from './HotelBoxParts/ShortOpinions';
import ShortAwards from './HotelBoxParts/ShortAwards';
import ShortClaims from './HotelBoxParts/ShortClaims';
import GraphicClaims from './HotelBoxParts/GraphicClaims';

import TopRatings from './HotelBoxParts/TopRatings';
import AvgPrice from './HotelBoxParts/AvgPrice';
import FeaturedCTF from './HotelBoxParts/FeaturedCTF';
import FeaturedCTFHeader from './HotelBoxParts/FeaturedCTFHeader';
import CardImageCarousel from './HotelBoxParts/CardImageCarousel';
import ViewHotelBtn from './HotelBoxParts/ViewHotelBtn';

const CollectionMember = (props) => {
    if(props.collectionmember !== null){
        return <div className="collection-member exists"><h2>Collection Member</h2><p>{props.collectionmember.getIn(['claimToFameType','description'])}</p></div>;
    }else{
        return <div className="collection-member"></div>;
    }
};
const ScoreCircle = (props) => {
    if(props.filteredAvgRating !== "0.00"){
        return <div className="stat-number">{props.filteredAvgRating === "0.00" ? '-' : props.filteredAvgRating}</div>;
    }else{
        return <div className="stat-number">{props.score === null ? '-' : props.score}</div>;
    }
};

const RatingFooter = (props) => {
    if(props.footerData !== "0.00"){
        return (
            <div className="recalc-footer">
                <p className="description">{"Recalculated score based on filtering"}</p>
                <div className="ratings-circle"><p>{props.footerData}</p></div>
            </div>
        );
    }else{
        return <p style={{display: 'none'}}></p>;
    }
}

const ScoreStar = (props) => {
    return (
        <div className="score-star">
            <p className="score-num">{props.score}</p>
        </div>
    );
}

export default class BoxView extends Component {

    // expand(e) {
    //     e.preventDefault();
    //     this.props.showCarousel(this.props.hotelId);
    // }

    shortOpinionsCheck(){
        const {shortOpinions, opinion} = this.props;
        if(shortOpinions !== 'null'){
            return <ShortOpinions opinionCount={opinion} shortOpinions={shortOpinions} />
        }
    }

    shortAwardsCheck(){
        const {shortAwards, awards} = this.props;
        if(shortAwards !== 'null'){
            return <ShortAwards awardCount={awards} shortAwards={shortAwards} />
        }
    }
    /*
    shortClaimsCheck(){
        const {claimsToFame, numClaimsToFame} = this.props;
        if(claimsToFame !== 'null' && claimsToFame !== null){
            return <ShortClaims claimsCount={numClaimsToFame} shortClaims={claimsToFame} />
        }
    }*/

    graphicClaimsCheck(){
        const {claimsToFame, numClaimsToFame} = this.props;
        if(claimsToFame !== 'null' && claimsToFame !== null){
            return <GraphicClaims claimsCount={numClaimsToFame} graphicClaims={claimsToFame} />
        }
    }

    hasHeaderFeaturedCTFCheck(){
        // const {featuredCTF} = this.props;

        // if(featuredCTF !== "null" && featuredCTF !== null){
        //     return <FeaturedCTFHeader type={featuredCTF.get('ctfDescription')} notes={featuredCTF.get('notes')} />
        // }
    }

    hasCollectionMemeber(){
        const { collectionMember } = this.props;
        if (collectionMember !== null) {
            return (
                <div className="avg-collection-section-wrapper">
                    <CollectionMember collectionmember={collectionMember} />
                    <AvgPrice isSingle={false}/>
                </div>
            );
        }else{
            return <AvgPrice isSingle={true}/>
        }
    }

    hasScoreChecker(scoreImg){
        const { topRatings, activeRatings, reviewDate } = this.props
        if(topRatings !== null){
            /*
            return(
                <div className="score-box-wrapper">
                  <div className="sg-sign for-score">
                    <img src={scoreImg} alt="score icon"/>
                     { this.hasScoreDescription(this.props.scoreDescription) }
                  </div>
                    <TopRatings activeRatings={activeRatings} topRatings={topRatings} reviewDate={reviewDate} />
                </div>
            );
            */
        }
    }

    hasScoreDescription(scoreDesc){
        if(scoreDesc !== null){
            return <div className="score-icon-text">MEDALS</div>
        }
    }

    trackVisit() {
        ga('send', 'event', 'Hotel Link', 'click', this.props.name, this.props.homepage);
    }

  render() {
      const scoreImg = "/assets/images/icons/sg-logo-black.svg";

      let hotelName = this.props.name;
      let affiliateButton = '';

      if( this.props.affiliatelink !== null ) {
          affiliateButton = (
            <a href={this.props.affiliatelink} target="_blank" className="website-link">Book Now</a>
          );
      }

      if (this.props.homepage != '') {
        hotelName = (
            <p><Link to={'/hotel/' + this.props.slug} className="hotel-link">{this.props.name}</Link><br/>
                <a href={this.props.homepage} target="_blank" className="website-link" onClick={() => this.trackVisit()}>Visit website</a>{'\u00A0\u00A0'}{affiliateButton}
            </p>
        );
      }

      let opened = null;
      if (this.props.opened) {
        opened = '. Opened: ' + this.props.opened;
      }

      let rooms = null;
      if (this.props.rooms) {
        rooms = (opened ? ',' : '.') + ' Rooms: ' + this.props.rooms;
      }

      // const ctfHeader = this.props.featuredCTF !== 'null' && this.props.featuredCTF !== null
      //   ? (<p className="sg-sign top-sign"> STAY GRAND </p>)
      //   : null;
      const ctfHeader = '';

    return (
        <div className="item hotel">
            <div className="item-header">
                <div className="item-header-top">
                    <CardImageCarousel hotelImages={this.props.hotelImages.toJS()}/>
                </div>
                <div className="item-header-bottom">
                    <div className="item-header-bottom-content">
                        <div className="info">
                            <h2 className="hotel-name">{hotelName}</h2>
                            <p><Link to={'/city/' + this.props.city.toLowerCase().replace(' ', '+')}>{this.props.city}</Link>, <Link to={'/country/' + this.props.country.toLowerCase().replace(' ', '+')}>{this.props.country}</Link>{opened}{rooms}</p>
                        </div>
                        {/* ctfHeader */}
                    </div>
                    {/* this.hasHeaderFeaturedCTFCheck() */}
                </div>
            </div>
            <div className="item-content">

                <div className="item-content-body">
                    { this.graphicClaimsCheck() }
                    { /*this.shortClaimsCheck()*/ }
                    { this.shortOpinionsCheck() }
                    { this.shortAwardsCheck() }
                    { this.hasScoreChecker(scoreImg) }

                </div>

            </div>
            <div className="item-content-footer">
                { this.hasCollectionMemeber() }
                <ViewHotelBtn slug={this.props.slug} />
            </div>
        </div>
    );
  }
}
// <Link to={'hotel/'+this.props.slug}></Link>
// { this.hasImagesCheck(this.props.hasImages) }
// { this.hasFeaturedCTFCheck() }
// <div className="cover-img">
//<TopRatings topRatings={this.props.topRatings} />
// </div>
