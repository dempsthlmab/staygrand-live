import React, { Component } from "react";
import Slider from "react-slick";

const PrevArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="award-arrow award-arrow-prev" onClick={onClick}>{" Previous"}</button>
};
const NextArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="award-arrow award-arrow-next" onClick={onClick}>{" Next"}</button>
};

export default class ShortAwardsCarousel extends Component {

    /*
    The first render of this component will not include any images so that is why we need
    to make a function that checks if there is any images.
    */

    loadImages(awards, settings){
        if(awards !== null){
            return(
                <Slider className="center" ref="slider" {...settings}>
                    {awards}
                </Slider>
            );
        }else{
            return <p style={{'display': 'none'}}></p>;
        }
    }

    render(){
        const settings = {
          dots: true,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px",
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
          afterChange: (slideNumber) => {
       //     this.setState({
       //         currentSlide: slideNumber
       //     });
          }
        };
        const {shortAwards} = this.props;

        let awards;
        if(shortAwards.size > 0){
            awards = shortAwards.map((award, index) => {
                const note = award.get('note') ? (<p className="opinion">{award.get('note')}</p>) : null;
                const category = award.get('category') ? (<p className="category">{award.get('category')}</p>) : null;

                return (
                    <div key={index} className="hotel-card-awards-carousel-item">
                        <h2 className="name">
                            {award.get('name')} {award.get('year')}
                        </h2>

                        {category}{note}

                        <p className="source">
                            {award.get('source')}
                        </p>

                    </div>
                );
            });
        }else{
            awards = null;
        }

        return(
            <div className="hotel-card-awards-carousel">
                <div className="hotel-card-awards-carousel-slider-wrapper">
                    <div className="hotel-card-awards-carousel-slider">
                        { this.loadImages(awards, settings) }
                    </div>
                </div>
            </div>
        );
    }
}
