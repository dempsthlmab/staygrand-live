import React, { Component } from "react";
import TopRatingCircle from './TopRatingCircle';

export default class SingleTopRatingChunk extends Component {
    render() {
        const {topRatings, activeRatings, reviewDate} = this.props;
        const ratingCircle = topRatings.map((rating, index) => {
            const ratingCode = rating.getIn(['getRatingType','code']);
            let activeClass = false;
            if(activeRatings.includes(ratingCode)){
                activeClass = true;
            }
           return <TopRatingCircle key={index+'_'+rating.get('type')} score={rating.get('rating')} scoreType={rating.get('getScoreType')} ratingType={rating.get('getRatingType')} activeClass={activeClass} note={rating.get('note')} reviewDate={reviewDate}/>
        });

      return (
          <div className="score-wrapper">
              {ratingCircle}
          </div>
      );
    }
}
