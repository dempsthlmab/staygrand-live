import React, { Component } from "react";

export default class SingleShortAward extends Component {
  render() {
      const { shortAwards } = this.props;
      const shortAward = shortAwards.first();

    const note = shortAward.get('note') ? (<p className="opinion">{shortAward.get('note')}</p>) : null;
    const category = shortAward.get('category') ? (<p className="category">{shortAward.get('category')}</p>) : null;

    return (
        <div className="single-short-award">
            <h2 className="name">{shortAward.get('name')} {shortAward.get('year')}</h2>
            {category}
            {note}
            <p className="source">{shortAward.get('source')}</p>
        </div>
    );
  }
}
