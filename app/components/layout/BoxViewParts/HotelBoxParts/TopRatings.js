import React, { Component } from "react";
import SingleTopRatingChunk from "./SingleTopRatingChunk";
import TopRatingsCarousel from "./TopRatingsCarousel";

import _ from 'lodash';

import TopRatingCircle from './TopRatingCircle';

export default class TopRatings extends Component {


  render() {
      const {topRatings, activeRatings, reviewDate} = this.props;
    //   let ratingCircle = ' ';
    //   console.log(topRatings);
    //   let chunkedTopRatings = _.chunk(topRatings.toArray(), 5);
    //   console.log(chunkedTopRatings);
    let componentType;
    let classType;
    if(topRatings !== null){
        if(topRatings.size <= 1){
            componentType = <SingleTopRatingChunk topRatings={topRatings} activeRatings={activeRatings} reviewDate={reviewDate} />
            classType = "score-box static";
        }else{
            componentType = <TopRatingsCarousel topRatings={topRatings} activeRatings={activeRatings} reviewDate={reviewDate} />
            classType = "score-box carousel";
        }
    }

    return (
        <div className={classType}>
            {componentType}
        </div>
    );
  }
}
