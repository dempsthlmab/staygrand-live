import React, { Component } from "react";
import ShortOpinionsCarousel from './ShortOpinionsCarousel';
import SingleShortOpinion from './SingleShortOpinion';



export default class ShortOpinions extends Component {

    hasOnlyOne(){
        const { shortOpinions, opinionCount } = this.props;
        if(opinionCount > 1){
            return <ShortOpinionsCarousel shortOpinions={shortOpinions} />
        }else{
            return <SingleShortOpinion shortOpinions={shortOpinions} />
        }
    }

  render() {
      const { shortOpinions, opinionCount } = this.props;
      const opinionImg = "/assets/images/icons/sg-opinion-black.svg";
      let isSingle = true;
      if(opinionCount > 1){
          isSingle = false;
      }

      if(opinionCount > 1){

      return (
          <div className="opinions-wrapper">

            <div className="sg-sign"><img src={opinionImg} alt="opinion icon"/><span>Praise</span>
            </div>
              <div className={ isSingle ? 'opinions single' : 'opinions' }>

                  <div className="content">
                      { this.hasOnlyOne() }
                  </div>

                  <div className="count">
                    <span>{opinionCount}</span>
                  </div>

              </div>

          </div>
      );

    } else {
      return (
          <div className="opinions-wrapper single">

            <div className="sg-sign"><img src={opinionImg} alt="opinion icon"/><span>Praise</span>
            </div>
              <div className={ isSingle ? 'opinions single' : 'opinions' }>

                  <div className="content">
                      { this.hasOnlyOne() }
                  </div>

              </div>

          </div>
      );
    }
  }
}
// <div className="link-wrapper">
//     <p className="link">
//         {'View all media opinions'}
//     </p>
// </div>
