import React, { Component } from "react";


export default class AvgPrice extends Component {

    shouldBreak(){
        const { isSingle } = this.props;
        if(!isSingle){
            return <br/>;
        }
    }

  render() {
      const { isSingle } = this.props;
    return (
        <div className={isSingle ? "avg-price-single-wrapper" : "avg-price-wrapper"}>
            <p className="avg-price">{'Average price for double'}{ this.shouldBreak() }{'rooms starts at 360€.'}</p>
        </div>
    );
  }
}
