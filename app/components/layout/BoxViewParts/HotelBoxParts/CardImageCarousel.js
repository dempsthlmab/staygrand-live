/*
HotelCardCarousel is the component that shows an image carousel of the hotel cards on the home page.
*/

import React, { Component } from "react";
import Slider from "react-slick";

export default class CardImageCarousel extends Component {

    /*
    The first render of this component will not include any images so that is why we need
    to make a function that checks if there is any images.
    */
    loadImages(images, settings){
        if(images !== null){
            return(
                <Slider className="center" ref="slider" {...settings}>
                    {images}
                </Slider>
            );
        }else{
            return <p style={{'display': 'none'}}></p>;
        }
    }

    render(){
        const settings = {
          dots: false,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px"
        };
        const hotelImages = this.props.hotelImages;
        let images;
        if(hotelImages.length > 0){
            images = hotelImages.map((image, index) => {
                // let image_url = image.get('imageUrl');
                // if(image_url.substring(0, 4) !== 'http'){
                //     image_url = 'https://www.staygrand.com'+image_url;
                // }
                return <div key={index}><div className="hotel-card-carousel-item" style={{backgroundImage: "url("+image+")", backgroundSize: "cover"}}></div></div>
            });
        }else{
            images = null;
        }

        return(
            <div className="hotel-card-carousel">
                <div className="hotel-card-carousel-slider-wrapper">
                    <div className="hotel-card-carousel-slider">
                        { this.loadImages(images, settings) }
                    </div>
                </div>
            </div>
        );
    }
}
