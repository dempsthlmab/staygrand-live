import React, { Component } from "react";

export default class SingleGraphicClaim extends Component {
  render() {
      const { graphicClaims } = this.props;
      const graphicClaim = graphicClaims.first();
    return (
        <div className="single-graphic-claim">
            <h2 className="name">
                {graphicClaim.get('claimToFameType').get('description')}
            </h2>
        </div>
    );
  }
}
