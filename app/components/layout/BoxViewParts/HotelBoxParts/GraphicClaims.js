import React, { Component } from "react";
import SingleGraphicClaim from './SingleGraphicClaim';

export default class GraphicClaims extends Component {

    hasOnlyOne(graphicClaims, awardCount){
        if(awardCount > 1){
            return <ShortAwardsCarousel graphicClaims={graphicClaims} />
        }else{
            return <SingleShortAward graphicClaims={graphicClaims} />
        }
    }

    hasClaims( graphicClaims, claimsCount ) {
        console.log( claimsCount );
        console.log( graphicClaims );
        let claims;
        claims = graphicClaims.map((claim, index) => {
            // const note = claim.get('note') ? (<p className="opinion">{claim.get('note')}</p>) : null;
            // const category = claim.get('category') ? (<p className="category">{claim.get('category')}</p>) : null;

            return (
                <div key={index} className="hotel-card-awards-carousel-item">
                    <h2 className="name">
                        {claim.get('claimToFameType').get('description')}
                    </h2>
                </div>
            );
        });

    }

    render() {
        const { graphicClaims, claimsCount } = this.props;
        const claimsImg = "/assets/images/icons/sg-logo-black.svg";  
       
        if (claimsCount > 0){
          return (
              <div className="awards-wrapper claims">
                
  
                  <div className="graphic-claims">
  
                      <div className="content">
                        { graphicClaims.map(function(claim) {
                            return (
                                <div className="claim-container">
                                    <h3 className="name">
                                        {claim.get('claimToFameType').get('description')}
                                    </h3>
                                </div>
                            );
                        })}
                      </div>
                      
                 </div>
              </div>
          );
      } 
      else {
          return null;
      }
    }

}
