import React, { Component } from "react";
import ShortAwardsCarousel from './ShortAwardsCarousel';
import SingleShortAward from './SingleShortAward';

export default class ShortAwards extends Component {

    hasOnlyOne(shortAwards, awardCount){
        if(awardCount > 1){
            return <ShortAwardsCarousel shortAwards={shortAwards} />
        }else{
            return <SingleShortAward shortAwards={shortAwards} />
        }
    }

  render() {
      const { shortAwards, awardCount } = this.props;
      const awardsImg = "/assets/images/icons/sg-awards-black.svg";
      let isSingle = true;
      if(awardCount > 1){
          isSingle = false;
      }

      if(awardCount > 1){

        return (
            <div className="awards-wrapper">
              <div className="sg-sign"><img src={awardsImg} alt="awards icon"/><span>Awards</span>
              </div>

                <div className={ isSingle ? 'awards single' : 'awards' }>

                    <div className="content">
                        { this.hasOnlyOne(shortAwards, awardCount) }
                    </div>

                    <div className="count">
                      <span>{awardCount}</span>
                    </div>

                </div>
            </div>
        );

    } else {
        return (
            <div className="awards-wrapper single">
              <div className="sg-sign"><img src={awardsImg} alt="awards icon"/><span>Awards</span>
              </div>

                <div className={ isSingle ? 'awards single' : 'awards' }>

                    <div className="content">
                        { this.hasOnlyOne(shortAwards, awardCount) }
                    </div>

                </div>
            </div>
        );
    }
  }
}
// <div className="link-wrapper">
//     <p className="link">
//         {'View all awards'}
//     </p>
// </div>
