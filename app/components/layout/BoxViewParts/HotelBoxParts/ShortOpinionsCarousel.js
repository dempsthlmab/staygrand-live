import React, { Component } from "react";

import Slider from "react-slick";

const PrevArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="opinion-arrow opinion-arrow-prev" onClick={onClick}>{" Previous"}</button>
};
const NextArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="opinion-arrow opinion-arrow-next" onClick={onClick}>{" Next"}</button>
};

const opinionSource = (props) => {
    const {sourceUrl, source} = props;
    if(sourceUrl !== ""){
        return (
            <a href={sourceUrl} target="_blank">
                {source}
            </a>
        );
    }else{
        return <p className="source">{source}</p>;
    }
}

export default class ShortOpinionsCarousel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentSlide: 0,
        };
    }

    /*
    The first render of this component will not include any images so that is why we need
    to make a function that checks if there is any images.
    */
    loadImages(opinions, settings){
        if(opinions !== null){
            return(
                <Slider className="center" ref="slider" {...settings}>
                    {opinions}
                </Slider>
            );
        }else{
            return <p style={{'display': 'none'}}></p>;
        }
    }

    renderSourceLink(sourceUrl, source, opinionDate){
        if(sourceUrl !== ""){
            return (
                <a className="source-link" href={sourceUrl} target="_blank">
                    {source}{opinionDate ? ', ' + opinionDate : ''}
                </a>
            );
        }else{
            return <p className="source">{source}</p>;
        }
    }

    renderTitle( title ) {
        if( title !== "" ) {
            return (
                <h2>{title}</h2>
            );
        }
    }

    render(){
        const settings = {
          dots: true,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px",
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
          afterChange: (slideNumber) => {
       //     this.setState({
       //         currentSlide: slideNumber
       //     });
          }
        };
        const {shortOpinions} = this.props;

        let opinions;
        if(shortOpinions.size > 0){
            opinions = shortOpinions.map((opinion, index) => {
                return (
                    <div key={index} className="hotel-card-opinions-carousel-item">
                        { this.renderTitle(opinion.get('title')) }
                        <p className="opinion">
                            {opinion.get('opinion')}
                        </p>
                        { this.renderSourceLink(opinion.get('sourceLink'), opinion.get('source'), opinion.get('opinionDate')) }
                    </div>
                );
            });
        }else{
            opinions = null;
        }

        return(
            <div className="hotel-card-opinions-carousel">
                <div className="hotel-card-opinions-carousel-slider-wrapper">
                    <div className="hotel-card-opinions-carousel-slider">
                        { this.loadImages(opinions, settings) }
                    </div>
                </div>
            </div>
        );
    }
}
