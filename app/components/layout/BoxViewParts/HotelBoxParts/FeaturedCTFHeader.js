import React, { Component } from "react";


export default class FeaturedCTFHeader extends Component {
  render() {
      const { type, notes } = this.props;
      const comments = notes.map((note, index) => {
          return <p key={"ctf_note_"+index}> {note}</p>;
      });
    return (
        <div className="item-content-header">
            <h2>{ type }</h2>
            { comments }
        </div>
    );
  }
}
