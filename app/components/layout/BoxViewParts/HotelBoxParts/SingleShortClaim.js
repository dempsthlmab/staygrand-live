import React, { Component } from "react";

export default class SingleShortClaim extends Component {
  render() {
      const { shortClaims } = this.props;
      const shortClaim = shortClaims.first();
    return (
        <div className="single-short-claim">
            <h2 className="name">
                {shortClaim.get('claimToFameType').get('description')}
            </h2>
            <p className="opinion">
                {shortClaim.get('note')}
            </p>
        </div>
    );
  }
}
