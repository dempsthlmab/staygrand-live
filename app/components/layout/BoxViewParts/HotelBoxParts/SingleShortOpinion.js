import React, { Component } from "react";

export default class SingleShortOpinion extends Component {

    renderSourceLink(sourceUrl, source, opinionDate){
        if(sourceUrl !== ""){
            return (
                <a className="source-link" href={sourceUrl} target="_blank">
                    {source}
                </a>
            );
        }else{
            return <p className="source">{source}{opinionDate ? ', ' + opinionDate : ''}</p>;
        }
    }

    renderTitle( title ) {
        if( title !== "" ) {
            return (
                <h2>{title}</h2>
            );
        }
    }

  render() {
      const { shortOpinions } = this.props;
      const shortOpinion = shortOpinions.first();
    return (
        <div className="single-short-opinion">
            { this.renderTitle(shortOpinion.get('title')) }
            <p className="opinion">
                {shortOpinion.get('opinion')}
            </p>
            { this.renderSourceLink(shortOpinion.get('sourceLink'), shortOpinion.get('source'), shortOpinion.get('opinion_date')) }
        </div>
    );
  }
}
