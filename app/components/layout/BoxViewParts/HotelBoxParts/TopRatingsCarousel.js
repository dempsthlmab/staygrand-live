import React, { Component } from "react";
import Slider from "react-slick";
import _ from 'lodash';
const { List } = require('immutable')

import TopRatingCircle from './TopRatingCircle';

const PrevArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="top-ratings-arrow top-rating-arrow-prev" onClick={onClick}>{" Previous"}</button>
};
const NextArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="top-ratings-arrow top-rating-arrow-next" onClick={onClick}>{" Next"}</button>
};

export default class TopRatingsCarousel extends Component {


    render(){
        const settings = {
          dots: true,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px",
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />
        };
        const {topRatings, activeRatings, reviewDate} = this.props;

        let chunkedTopRatings = _.chunk(topRatings.toArray(), 1);
        chunkedTopRatings = List(chunkedTopRatings);
        const topRatingSlides = chunkedTopRatings.map((topRatingChunks, index) =>{
            const topRatingChunk = topRatingChunks.map((topRating, index) => {
                const ratingCode = topRating.getIn(['getRatingType','code']);
                let activeClass = false;
                if(activeRatings.includes(ratingCode)){
                    activeClass = true;
                }
               return <TopRatingCircle key={index+'_'+topRating.get('type')} score={topRating.get('rating')} scoreType={topRating.get('getScoreType')} ratingType={topRating.get('getRatingType')} activeClass={activeClass} note={topRating.get('note')} reviewDate={reviewDate} />
            });
            return (
                <div key={index} className="hotel-card-top-ratings-carousel-item">
                    {topRatingChunk}
                </div>
            );
        });


        return(
            <div className="hotel-card-top-ratings-carousel">
                <div className="hotel-card-top-ratings-carousel-slider-wrapper">
                    <div className="hotel-card-top-ratings-carousel-slider">
                        <Slider className="center" ref="slider" {...settings}>
                            {topRatingSlides}
                        </Slider>
                    </div>
                    <div className="count">
                      <span>{topRatings.size}</span>
                    </div>

                </div>
            </div>

        );
    }
}
// <div className="hotel-card-top-ratings-carousel">
//     <div className="hotel-card-top-ratings-carousel-slider-wrapper">
//         <div className="hotel-card-top-ratings-carousel-slider">
//             { this.loadImages(TopRatings, settings) }
//         </div>
//     </div>
// </div>
