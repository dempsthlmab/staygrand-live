import React, { Component } from "react";
import Slider from "react-slick";

const PrevArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="award-arrow award-arrow-prev" onClick={onClick}>{" Previous"}</button>
};
const NextArrow = (props) => {
    const {onClick} = props;
    return <button type="button" className="award-arrow award-arrow-next" onClick={onClick}>{" Next"}</button>
};

export default class ShortClaimsCarousel extends Component {

    /*
    The first render of this component will not include any images so that is why we need
    to make a function that checks if there is any images.
    */

    loadImages(awards, settings){
        if(awards !== null){
            return(
                <Slider className="center" ref="slider" {...settings}>
                    {awards}
                </Slider>
            );
        }else{
            return <p style={{'display': 'none'}}></p>;
        }
    }

    render(){
        const settings = {
          dots: true,
          autoplay: false,
          infinite: true,
          slidesToShow: 1,
          draggable: true,
          arrows: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: "0px",
          nextArrow: <NextArrow />,
          prevArrow: <PrevArrow />,
          afterChange: (slideNumber) => {
       //     this.setState({
       //         currentSlide: slideNumber
       //     });
          }
        };
        const {shortClaims} = this.props;

        let claims;
        if(shortClaims.size > 0){
            claims = shortClaims.map((claim, index) => {
                // const note = claim.get('note') ? (<p className="opinion">{claim.get('note')}</p>) : null;
                // const category = claim.get('category') ? (<p className="category">{claim.get('category')}</p>) : null;

                return (
                    <div key={index} className="hotel-card-awards-carousel-item">
                        <h2 className="name">
                            {claim.get('claimToFameType').get('description')}
                        </h2>
                        <p className="claimopinion">
                            {claim.get('note')}
                        </p>
                    </div>
                );
            });
        }else{
            claims = null;
        }

        return(
            <div className="hotel-card-awards-carousel">
                <div className="hotel-card-awards-carousel-slider-wrapper">
                    <div className="hotel-card-awards-carousel-slider">
                        { this.loadImages(claims, settings) }
                    </div>
                </div>
            </div>
        );
    }
}
