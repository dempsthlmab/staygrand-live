import React, { Component } from "react";
import ShortClaimsCarousel from './ShortClaimsCarousel';
import SingleShortClaim from './SingleShortClaim';

export default class ShortClaims extends Component {

    hasOnlyOne(shortClaims, claimsCount){
        if(claimsCount > 1){
            return <ShortClaimsCarousel shortClaims={shortClaims} />
        }else{
            return <SingleShortClaim shortClaims={shortClaims} />
        }
    }

  render() {
      const { shortClaims, claimsCount } = this.props;
      const claimsImg = "/assets/images/icons/sg-logo-black.svg";
      let isSingle = true;

      if (claimsCount > 1) {
          isSingle = false;
      }

      if (claimsCount > 1){
        return (
            <div className="awards-wrapper claims">
              <div className="sg-sign sg"><img src={claimsImg} alt="claims icon"/><span>Claim to Fame</span>
              </div>

                <div className={ isSingle ? 'awards single' : 'awards' }>

                    <div className="content">
                        { this.hasOnlyOne(shortClaims, claimsCount) }
                    </div>

                    <div className="count">
                      <span>{claimsCount}</span>
                    </div>

                </div>
            </div>
        );
    } else if (claimsCount == 1) {
        return (
            <div className="awards-wrapper claims single">
              <div className="sg-sign sg"><img src={claimsImg} alt="claims icon"/><span>Claim to Fame</span>
              </div>

                <div className={ isSingle ? 'claims single' : 'claims' }>

                    <div className="content">
                        { this.hasOnlyOne(shortClaims, claimsCount) }
                    </div>

                </div>
            </div>
        );
    }
    else {
        return null;
    }
  }
}
// <div className="link-wrapper">
//     <p className="link">
//         {'View all awards'}
//     </p>
// </div>
