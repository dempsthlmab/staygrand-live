import React, { Component } from "react";
import { Link } from 'react-router';


export default class ViewHotelBtn extends Component {
  render() {
      const {slug} = this.props;
    return (

        <div className="view-hotel-btn-wrapper">
            <Link to={'hotel/'+slug}>{this.props.name}
                <button className="view-hotel-btn">View Hotel</button>
            </Link>
        </div>
    );
  }
}
