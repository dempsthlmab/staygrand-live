import React, { Component } from "react";

const RatingDescription = (props) =>{
    const {score} = props;
    if(score === 8){
        return <p className="rating-descripton">&nbsp;{"Top Class"}</p>;
    }else if(score === 9){
        return <p className="rating-descripton">&nbsp;{"World Class"}</p>;
    }else if(score === 10){
        return <p className="rating-descripton">&nbsp;{"World Best"}</p>;
    }
}

export default class TopRatingCircle extends Component {
    render() {
        const { score, scoreType, ratingType, activeClass, note, reviewDate } = this.props;

        const theRatingType = ratingType
            ? (<p className="rating-type">{ratingType.get('name')}{':'}</p>)
            : null;

        const noteParagraph = note ? (<p className="rating-note">{note}</p>) : null;

        let ratingImage = null;
        switch (score) {
            case 10:
                ratingImage = '/assets/images/icons/gold.png';
            break;

            case 9:
                ratingImage = '/assets/images/icons/silver.png';
            break;

            case 8:
                ratingImage = '/assets/images/icons/bronze.png';
            break;

            default:
                ratingImage = '/assets/images/icons/gold.png';
            break;
        }

        return (
            <div className="top-rating-box">
                <p className={ activeClass ? 'top-rating-circle active' : 'top-rating-circle' }><img src={ratingImage} /></p>
                <div className="top-rating-info">
                    {theRatingType}
                    <RatingDescription score={this.props.score} />
                    {noteParagraph}
                    <p>{reviewDate}</p>
                </div>
            </div>
        );
    }
}
// <p className="rating-descripton">{scoreType.get('description')}</p>
