import React, { Component } from "react";


export default class FeaturedCTF extends Component {
  render() {
      const { type, notes } = this.props;
      const comments = notes.map((note, index) => {
          return <p key={"ctf_note_"+index}> {note}</p>;
      });
    return (
        <div className="ctf-overlay">
            <h3>{type}</h3>
            {comments}
            <div className="arrow"></div>
        </div>
    );
  }
}
