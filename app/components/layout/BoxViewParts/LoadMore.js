import React, { Component } from "react";

export default class LoadMore extends Component {
    loadMoreCheck(){
        if(this.props.nextUrlExists){
            return (
                <div>
                    <button onClick={this.props.loadMoreFunction} className="load-more">Load More</button>
                    <div className="sp">
                        <div className="sp-3balls"></div>
                    </div>
                </div>
            );
        }
    }
  render() {
    return (
        <div className={this.props.isLoadingMore ? "load-more-wrapper loading" : "load-more-wrapper"}>
            { this.loadMoreCheck() }
        </div>
    );
  }
}
