import React from 'react';
import compose from 'recompose/compose';
import defaultProps from 'recompose/defaultProps';
import withState from 'recompose/withState';
import withHandlers from 'recompose/withHandlers';
import withPropsOnChange from 'recompose/withPropsOnChange';
import GoogleMapReact from 'google-map-react';
import ClusterMarker from './MapViewParts/ClusterMarker';
import SimpleMarker from './MapViewParts/SimpleMarker';
import MapMarker from './MapViewParts/MapMarker';
import supercluster from 'points-cluster';
import {connect} from 'react-redux';

import { fitBounds } from 'google-map-react/utils';

// import { fetchMapHotels } from 'actions/questions';

export const gMap = ({
  style, hoverDistance, options,
  // center, zoom,
  mapCenter: { center, zoom },
  onChange, onChildMouseEnter, onChildMouseLeave, onChildClick, onCloseClick,
  activeMarker, popupHotel, fetchMapHotel, resetMapHotel,
  clusters,
}) => {
return (
  <GoogleMapReact
    bootstrapURLKeys={{
      key: 'AIzaSyDdvkleY5cNjvxO4cTj-Vn1toU4uDNPI0w',
      language: 'en',
    }}
    style={style}
    options={options}
    hoverDistance={hoverDistance}
    center={center}
    zoom={zoom}
    onChange={onChange}
    onChildMouseEnter={onChildMouseEnter}
    onChildMouseLeave={onChildMouseLeave}
    onChildClick={onChildClick}
  >
      {
        clusters
          .map(({ ...markerProps, id, numPoints }, index) => (
            numPoints === 1
              ? <SimpleMarker key={id} {...markerProps} fetchMapHotel={fetchMapHotel} popupHotel={popupHotel} onCloseClick={onCloseClick}/>
              : <ClusterMarker key={id} {...markerProps} />
          ))
      }
  </GoogleMapReact>);
};

// const activeHotelState = ({activeMarker}) => {
//   return activeMarker;
// };

// const setMapVals = ({center, zoom}) => {
//   return {center, zoom}
// };

export const gMapHOC = compose(
    defaultProps({
        clusterRadius: 100,
        hoverDistance: 30,
        options: {
            minZoomOverride: true,
            minZoom: 3,
            maxZoom: 18,
            scrollwheel: false,
            gestureHandling: 'cooperative',
            clickableIcons: false,
            styles: [
                {
                    "featureType":"administrative",
                    "elementType":"all",
                    "stylers":[
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType":"administrative.province",
                    "elementType":"all",
                    "stylers":[
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType":"landscape",
                    "elementType":"all",
                    "stylers":[
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 65
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType":"poi",
                    "elementType":"all",
                    "stylers":[
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": "50"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType":"road",
                    "elementType":"all",
                    "stylers":[
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType":"road.highway",
                    "elementType":"all",
                    "stylers":[
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType":"road.arterial",
                    "elementType":"all",
                    "stylers":[
                        {
                            "lightness": "30"
                        }
                    ]
                },
                {
                    "featureType":"road.local",
                    "elementType":"all",
                    "stylers":[
                        {
                            "lightness": "40"
                        }
                    ]
                },
                {
                    "featureType":"transit",
                    "elementType":"all",
                    "stylers":[
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType":"water",
                    "elementType":"geometry",
                    "stylers":[
                        {
                            "hue": "#ffff00"
                        },
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -97
                        }
                    ]
                },
                {
                    "featureType":"water",
                    "elementType":"labels",
                    "stylers":[
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -100
                        }
                    ]
                }
            ]
        },
        style: {
            position: 'relative',
            margin: 0,
            padding: 0,
            display: "block",
            height: "100vh",
            width: "100%"
        },
    }),

    // withState(
    //     'hoveredMarkerId',
    //     'setHoveredMarkerId',
    //     -1
    // ),
    withState(
        'isDraggable',
        'setDraggable',
        true
    ),
    // withState(
    //     "clickedMarkerId",
    //     "setClickedMarkerId",
    //     activeHotelState
    // ),
    withState(
        'mapProps',
        'setMapProps',
        ({ center, zoom, bounds }) => ({ center, zoom, bounds })
    ),
    withState(
        'mapCenter',
        'setMapCenter',
        ({ center, zoom }) => ({ center, zoom })
    ),
    withHandlers({
        onChange: ({ fetchMapHotelsInBounds, setMapProps }) => ({ center, zoom, bounds }) => {
            if (bounds) {
                fetchMapHotelsInBounds(bounds.nw, bounds.se);
            }

            setMapProps({ center, zoom, bounds });
        },

        onChildMouseEnter: ({ setHoveredMarkerId }) => (hoverKey, { id }) => {
            // setHoveredMarkerId(id);
        },

        onChildMouseLeave: ({ setHoveredMarkerId }) => (/* hoverKey, childProps */) => {
            // setHoveredMarkerId(-1);
        },

        onChildClick: ({ setClickedMarkerId, clickedMarkerId, setMapCenter, setDraggable, fetchMapHotel, resetMapHotel, mapCenter }) => (event, { id, lat, lng, numPoints, slug, sum }) => {
            const center = {lat, lng};

            if (numPoints === 1) {
                fetchMapHotel(slug);
                setDraggable(false);
                // setMapCenter({ center, zoom: mapCenter.zoom })

                // if (clickedMarkerId === id) {
                //     setClickedMarkerId(-1);
                //     // setDraggable(true);
                // } else {
                //     setClickedMarkerId(id);
                //     // setMapCenter({ center });
                //     // setDraggable(false);
                // }
            } else {
                if (numPoints > 1) {
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0; i < sum.length; i++) {
                        bounds.extend({
                            lat: sum[i].lat,
                            lng: sum[i].lng,
                        });
                    }

                    const size = {
                      width: window.innerWidth, // Map width in pixels
                      height: window.innerHeight, // Map height in pixels
                    };

                    var fit = fitBounds({ ne: { lat: bounds.getNorthEast().lat(), lng: bounds.getNorthEast().lng() }, sw: { lat: bounds.getSouthWest().lat(), lng: bounds.getSouthWest().lng() } }, size);

                    setMapCenter({ center: fit.center, zoom: fit.zoom })

                }

                resetMapHotel();
                setDraggable(true);
                // if (clickedMarkerId !== -1) {
                    // setClickedMarkerId(-1);
                    // setDraggable(true);
                // }
            }
        },
        onCloseClick: ({ setClickedMarkerId, setDraggable, resetMapHotel }) => (event) => {
            resetMapHotel();
            setDraggable(true);
        },
    }),
    // precalculate clusters if markers data has changed
    // withPropsOnChange(
    //     ['mapCenter'],
    //     ({ Hotels = [], clusterRadius, options: { minZoom, maxZoom } }) => ({
    //         getCluster: supercluster(
    //             Hotels,
    //             {
    //                 minZoom, // min zoom to generate clusters on
    //                 maxZoom, // max zoom level to cluster the points on
    //                 radius: clusterRadius, // cluster radius in pixels
    //             }
    //         ),
    //     })
    // ),

    withPropsOnChange(
        ['hotels'],
        ({ hotels = [], clusterRadius, options: { minZoom, maxZoom } }) => ({
            getCluster: supercluster(
                hotels.toJS(),
                {
                    minZoom, // min zoom to generate clusters on
                    maxZoom, // max zoom level to cluster the points on
                    radius: clusterRadius, // cluster radius in pixels
                }
            ),
        })
    ),
    // get clusters specific for current bounds and zoom
    withPropsOnChange(
        ['mapProps', 'getCluster'],
        ({ mapProps, getCluster }) => ({
            clusters: mapProps.bounds
                ? getCluster(mapProps)
                    .map(({ wx, wy, numPoints, points }) => ({
                        lat: wy,
                        lng: wx,
                        text: numPoints,
                        numPoints,
                        sum: points,
                        id: `${numPoints}_${points[0].slug}`,
                        slug: points[0].slug
                    }))
                    : [],
        })
    ),
    withPropsOnChange((props, nextProps) => {
        if (props.center.lat != nextProps.center.lat || props.center.lng != nextProps.center.lng || props.zoom != nextProps.zoom) {
            return true
        }

        return false
    }, ({ setMapCenter, zoom, center, popupHotel }) => {
        // if (!popupHotel) {
            setMapCenter({ center, zoom });
        // }
    }),
    withPropsOnChange([ 'popupHotel' ], ({ popupHotel, setMapCenter, mapCenter }) => {
        if (popupHotel) {
            var center = {
                lat: popupHotel.get('lat'),
                lng: popupHotel.get('lng')
            };
            setMapCenter({ center, zoom: mapCenter.zoom });
        }
    }),
    // set hovered
    // withPropsOnChange(
    //     ['clusters', 'hoveredMarkerId'],
    //     ({ clusters, hoveredMarkerId }) => ({
    //         clusters: clusters
    //         .map(({ ...cluster, id }) => ({
    //             ...cluster,
    //             hovered: id === hoveredMarkerId,
    //         })),
    //     })
    // ),
    //set clicked
    // withPropsOnChange(
    //     ['clusters', 'clickedMarkerId'],
    //     ({ clusters, clickedMarkerId }) => ({
    //         clusters: clusters
    //         .map(({ ...clusters, id }) => ({
    //             ...clusters,
    //             clicked: id === clickedMarkerId,
    //         })),
    //     })
    // ),
    withPropsOnChange(
        ['isDraggable', 'options'],
        ({ isDraggable, options }) => {
            return {
                options: { ...options, gestureHandling: isDraggable ? 'cooperative' : 'none' }
            };
        }
    )
);
export default gMapHOC(gMap);
