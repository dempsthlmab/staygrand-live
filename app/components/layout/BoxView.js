/*
BoxView component.
This is the view that shows the hotel cards
*/

import React, { Component } from "react";
import { List } from 'immutable';
import _ from 'lodash';

import HotelBox from "./BoxViewParts/HotelBox";
import LoadMore from "./BoxViewParts/LoadMore";
import ScrollTopBtn from "./BoxViewParts/ScrollTopBtn";

export default class BoxView extends Component {

    render() {
        const { Hotels, activeRatings } = this.props;
        let footerData = null;
        if(activeRatings.size !== 0){
            const totalRatings = activeRatings.size;
            footerData = activeRatings.map((rating, index) => {
                if(totalRatings === index + 1){
                    return rating;
                }else{
                    return rating+", ";
                }

            });
        }
        let HotelComponents = "";
        if(Hotels.size !== 0){
            HotelComponents = Hotels.map( hotelArrays => hotelArrays.map(hotel => {
                let hasImages = false;
                if(hotel.get('allImages').size > 0){
                    hasImages = true;
                }

                return <HotelBox key={hotel.get('hotelId')} affiliatelink={hotel.get('affiliatelink')} hotelId={hotel.get('hotelId')} name={hotel.get('name')} slug={hotel.get('slug')} city={hotel.get('cityPlace')} country={hotel.get('country')} hotelImages={hotel.get('allImages')} score={hotel.get('avgRating')} awards={hotel.get('numAwards')} opinion={hotel.get('numOpinions')} numClaimsToFame={hotel.get('numClaimsToFame')} collectionMember={hotel.get('collectionMember')} filteredAvgRating={hotel.get('ratingSumScore')} footerData={footerData} shortAwards={hotel.get('shortAwards')} shortOpinions={hotel.get('shortOpinions')} topRatings={hotel.get('topRatings')} claimsToFame={hotel.get('claimsToFame')} hasImages={hasImages} scoreDescription={hotel.get('scoreDescription')} activeRatings={activeRatings} homepage={hotel.get('homepage')} opened={hotel.get('yearOpened')} rooms={hotel.get('rooms')} reviewDate={hotel.get('reviewDate')} />;
            }));
        }
        return (
            <div className="box-view">
                <div className="containery">
                    <div className="grid">
                        { HotelComponents }
                    </div>
                    <LoadMore loadMoreFunction={this.props.loadMoreFunction} isLoadingMore={this.props.isLoadingMore} nextUrlExists={this.props.nextUrlExists} />
                </div>
                <ScrollTopBtn />
            </div>
        );
    }
}
