import React, { Component } from "react";
import { connect } from 'react-redux';

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

class TotalHotels extends Component {
    render() {

        var placeName = this.props.siteState.search.searchVal ? capitalizeFirstLetter(this.props.siteState.search.searchVal) : 'the World';
        var totalVal = this.props.total == 1 ? 'the' : (<span>{this.props.total}</span>);

        if (this.props.singleHotel) {
            return (
                <div className="total-result-wrapper">
                    <div className="containery">
                        <div className="total-result">
                            <p>Results: <span>{this.props.total}</span></p>
                        </div>
                        <div className="introtext">
                            <p>Dear Hotel Lover!</p>
                            <p>StayGrand is a Global Hotel Guide based on the most honored Awards from the travel industry, combined with Praise given by the most prestigious publications around the world. Simply the most inspirational hotel guide there is.</p>
                            <p>Go explore and StayGrand!</p>
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div className="total-result-wrapper">
                <div className="containery">
                    <div className="total-result">
                        <p>Discover {totalVal} {this.props.total != 1 ? 'of the' : null} best hotel{this.props.total != 1 ? 's' : null} in <span>{placeName}</span></p>
                    </div>
                    <div className="introtext">
                        <p>Dear Hotel Lover!</p>
                        <p>StayGrand is a Global Hotel Guide based on the most honored Awards from the travel industry, combined with Praise given by the most prestigious publications around the world. Simply the most inspirational hotel guide there is.</p>
                        <p>Go explore and StayGrand!</p>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        siteState: state.siteState.toJS(),
        hotels: state.hotels.toJS(),
    }
}

export default connect(
    mapStateToProps,
    {

    }
)(TotalHotels)
