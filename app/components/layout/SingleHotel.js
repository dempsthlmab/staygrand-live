import React, { Component } from "react";

import Header from "./SingleViewParts/Header";
import HeroComponent from "./SingleViewParts/HeroComponent";
import HotelGeneralInfo from "./SingleViewParts/HotelGeneralInfo";
import ClaimToFames from "./SingleViewParts/ClaimToFames";
import HotelMiniMap from "./SingleViewParts/HotelMiniMap";
import HotelAwards from "./SingleViewParts/HotelAwards";
import HotelMedia from "./SingleViewParts/HotelMedia";
import RatingsSection from "./SingleViewParts/RatingsSection";

export default class SingleHotel extends Component {

    renderRatingsSection(review, ratings, avgRating){
        if (this.props.Hotel.get('reviews') !== null) {
            return (
                <div>
                    <RatingsSection ratingsInfo={ratings} review={review} allAvgRating={avgRating}/>
                </div>
            );
        }
    }

  render() {
      const totalMedia = this.props.Hotel.get('numOpinions');
    return (
        <div>
            <Header />
            <HeroComponent Hotel={this.props.Hotel} Images={this.props.Hotel.get('images')}/>
            <div className="general-ctf-section">
                <div className="containery">
                    <HotelGeneralInfo Hotel={this.props.Hotel} loadMap={this.props.loadMap}/>
                    <ClaimToFames claimtofames={this.props.Hotel.get('claimsToFames')} />
                </div>
            </div>
            <HotelMedia opinions={this.props.Hotel.get('opinions')} totalMedia={totalMedia}/>
            <HotelAwards awards={this.props.Hotel.get('awards')} numAwards={this.props.Hotel.get('numAwards')}/>
            { this.renderRatingsSection(this.props.Hotel.get('reviews'), this.props.ratings, this.props.Hotel.get('avgRatingsList'))  }
        </div>
    );
  }
}
 // numAwards={this.props.Hotel.get('numAwards')}
// { this.renderRatingsSection(this.props.Hotel.get('reviews'), this.props.ratings, this.props.Hotel.get('name')) }
