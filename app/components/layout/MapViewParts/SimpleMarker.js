import React from 'react';
import compose from 'recompose/compose';
import defaultProps from 'recompose/defaultProps';
import withPropsOnChange from 'recompose/withPropsOnChange';
import { Motion } from 'react-motion';
import { clusterMarkerHOC } from './ClusterMarker.js';
import ViewHotelButton from "./ViewHotelButton";
import { fromJS, Map } from 'immutable';

import HotelBox from "../BoxViewParts/HotelBox";

export const simpleMarker = ({
  styles,
  defaultMotionStyle, motionStyle,
  images, sum, usedClass, activeClass, onCloseClick, popupHotel, slug
}) => {
    const hotel = popupHotel;

    const stopPropagation = (event) => {
        event.stopPropagation();
        return false;
    }

  return (<Motion
    defaultStyle={defaultMotionStyle}
    style={motionStyle}
  >
  {
    ({ scale }) => {
        const hasImages = true;

        const footerData = null;
        const activeRatings = [];

        return (<div className={hotel && hotel.get('slug') === slug ? activeClass : usedClass}>

            {hotel && hotel.get('slug') === slug ? <div className="map-popup">
              <div className="close-wrap">
                <div className="close-popup-bg"></div>
                <button className="close-popup" type="button" onClick={onCloseClick}></button>
              </div>
              <div className="hotel-box-container" onClick={stopPropagation} onTouchMove={stopPropagation} >
                <HotelBox key={hotel.get('hotelId')} hotelId={hotel.get('hotelId')} name={hotel.get('name')} slug={hotel.get('slug')} city={hotel.get('cityPlace')} country={hotel.get('country')} hotelImages={hotel.get('allImages')} score={hotel.get('avgRating')} awards={hotel.get('numAwards')} opinion={hotel.get('numOpinions')} numClaimsToFame={hotel.get('numClaimsToFame')} collectionMember={hotel.get('collectionMember')} filteredAvgRating={hotel.get('ratingSumScore')} footerData={footerData} shortAwards={hotel.get('shortAwards')} shortOpinions={hotel.get('shortOpinions')} topRatings={hotel.get('topRatings')} claimsToFame={hotel.get('claimsToFame')} hasImages={hasImages} scoreDescription={hotel.get('scoreDescription')} activeRatings={activeRatings} homepage={hotel.get('homepage')} opened={hotel.get('yearOpened')} rooms={hotel.get('rooms')} reviewDate={hotel.get('reviewDate')} />
              </div>
            </div> : null}
            <div
              className="single-marker"
              style={{
                transform: `translate3D(0,0,0) scale(${scale}, ${scale})`,
              }}
            >
            </div>
        </div>);
    }
  }
  </Motion>);
};

export const simpleMarkerHOC = compose(
  defaultProps({
    // initialScale: 0.3,
    // defaultScale: 0.8,
    // hoveredScale: 0.9,
    clicked: false,
    activeClass: "map-icon-wrapper active",
    deactiveClass: "map-icon-wrapper",
    usedClass: "map-icon-wrapper",
    images: {
        scoreImg: "../../../assets/images/icons/sg-logo-black.svg",
        awardsImg: "../../../assets/images/icons/sg-awards-black.svg",
        opinionImg: "../../../assets/images/icons/sg-opinion-black.svg",
        crossImg: "../../../assets/images/icons/hotel-card-cross.svg",
    },
  }),
  // withPropsOnChange(
  //   ['clicked'],
  //   ({ clicked, deactiveClass, activeClass }) => ({
  //     clicked,
  //     usedClass: clicked ? activeClass : deactiveClass,
  //   })
  // ),
  // resuse HOC
  clusterMarkerHOC
);

export default simpleMarkerHOC(simpleMarker);

// <div className="close-popup-wrapper">
