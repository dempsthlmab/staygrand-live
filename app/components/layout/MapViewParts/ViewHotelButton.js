import React, { Component } from "react";
import { Link } from 'react-router';


export default class ViewHotelButton extends Component {

  render() {
    return (

        <div className="map-popup-footer">
            <Link to={'hotel/'+this.props.Slug}>
                view hotel
            </Link>
        </div>
    );
  }
}
