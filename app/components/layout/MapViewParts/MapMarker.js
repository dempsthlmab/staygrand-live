import React, { Component } from 'react';
export default class MapMarker extends Component {

  render() {
      const scoreImg = "../../../assets/images/icons/score-icon-black.png";
      const awardsImg = "../../../assets/images/icons/awards-icon-black.png";
      const opinionImg = "../../../assets/images/icons/opinion-icon-black.png";
    return (
        <div className="single-map-icon-wrapper">
            <div className="single-hotel-marker"></div>
        </div>

    );
  }
}
