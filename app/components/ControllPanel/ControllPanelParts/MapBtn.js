import React, { Component } from "react";

export default class MapBtn extends Component {
    updateView(){
        const view = 'Map';
        this.props.toggleView(view);
    }
  render() {
    return (
      <div className="view-buttons-wrapper">
        <button onClick = {this.updateView.bind(this)} className="mapview-button"></button>
      </div>
    );
  }
}
