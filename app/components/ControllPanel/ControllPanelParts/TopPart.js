import React, { Component } from 'react';
// import { Link } from "react-router";
//
import SearchField from "./SearchField";
import MapBtn from  "./MapBtn";
import BoxBtn from "./BoxBtn";
import FilterBtn from  "./FilterParts/FilterBtn";


export default class TopPart extends Component {

    render() {
        var viewBtn;
        if(this.props.viewType === 'Box'){
            viewBtn = <MapBtn toggleView={this.props.toggleView} />;

        }else if(this.props.viewType === 'Map'){
            viewBtn = <BoxBtn toggleView={this.props.toggleView} />;
        }
        const hasResponse = this.props.searchResponse !== null ? true : false;
        return (
            <div className="controll-panel-top-part">
                <div className="containery full-strech">
                    <div className="controll-panel-top-wrapper">
                        <div className="controll-panel-top">
                            <SearchField loadSearchFilteredHotels={this.props.loadSearchFilteredHotels} searchResponse={this.props.searchResponse} searchIcon={this.props.searchIcon} searchVal={this.props.searchVal} searchFilter={this.props.searchFilter} searchReset={this.props.searchReset} sgSearch={this.props.sgSearch} hasResponse={hasResponse} setDefaultSearch={this.props.setDefaultSearch}/>
                            { viewBtn }
                             <FilterBtn filterCollapseState={this.props.filterCollapseState} onFilterBtnClick={this.props.onFilterBtnClick} activeFilters={this.props.activeFilters}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
