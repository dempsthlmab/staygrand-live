import React, { Component } from "react";

export default class DefaultIcon extends Component {
  render() {
    return (
        <div className="search-icon-wrap">
            <div className="search-icon">
                <div className="glass">
                      <i></i><i></i><i></i>
                </div>
                <i className="handle"></i>
            </div>
        </div>
    );
  }
}
