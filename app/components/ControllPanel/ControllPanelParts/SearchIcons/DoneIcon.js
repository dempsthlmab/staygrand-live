import React, { Component } from "react";

export default class DoneIcon extends Component {
  render() {
    return (
        <div className="erase-wrap" onClick={(event) => {this.props.searchReset()}}>
            <div className="erase">
                <i></i><i></i>
            </div>
        </div>
    );
  }
}
