import React, { Component } from "react";

export default class LoadingIcon extends Component {
  render() {
    return (
        <div className="spinner-wrap">
            <div className="spinner">
                <i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i>
            </div>
        </div>
    );
  }
}
