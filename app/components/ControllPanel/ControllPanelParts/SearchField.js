import React, { Component } from "react";
import { Link } from 'react-router';
import SearchIcons from './SearchIcons';
import { Map } from 'immutable';

import { browserHistory } from 'react-router';

export default class SearchField extends Component {
    constructor(props){
        super(props);
        this.state = {
            doneTypeingInterval: 5000,
            activeTypingTimer: '',
            preventBlur: false,
            slideDown: false,
            searchVal: props.searchVal,
            slideUpTimer: '',
        };
        this.typingTimer = '';
    }

    componentWillUnmount(){
        //killes the slideUp timer so it does not try to slide up when the component are not mounted
        clearTimeout(this.state.slideUpTimer);
        if(this.props.searchFilter.size === 0 && this.props.hasResponse){
            this.props.setDefaultSearch();
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.searchVal != this.props.searchVal) {
            this.setState({ searchVal: nextProps.searchVal });
        }
    }

  render() {
      let continentResult = '';
      let countryResult = '';
      let areaResult = '';
      let cityResult = '';
      let hotelResult = '';
      let articleResult = '';
      let noResponseMsg = '';

      if(this.props.searchResponse !== null){
          const searchResponse = this.props.searchResponse;
          const hasHotels = searchResponse.get('hotels').size;
          const hasArticle = searchResponse.get('articles').size;
          const hasAreaIsland = searchResponse.getIn(['locations', 'areaIsland']).size
          const hasCity = searchResponse.getIn(['locations', 'cityPlace']).size;
          const hasCountry = searchResponse.getIn(['locations', 'country']).size;
          const hasContinent = searchResponse.getIn(['locations', 'continent']).size;
          if(!hasHotels && !hasArticle && !hasAreaIsland && !hasCity && !hasCountry && !hasContinent){
              noResponseMsg = <div className="no-response-message-wrapper"><p className="no-response-message">{'Your search did not return any results'}</p></div>
          }else{
              if(searchResponse.get('hotels').size > 0){
                  hotelResult = searchResponse.get('hotels').map((hotel, index) => {
                    return <p key={index}><a onClick={this.handleClick.bind(this, hotel.get('slug'), 'hotel')}>{hotel.get('name')}</a></p>
                    // return <p key={hotel.get('hotelId')}><Link to={'hotel/'+hotel.get('slug')}>{hotel.get('name')}</Link></p>
                  });
                  hotelResult = <div className="column"><h3>Hotels</h3>{hotelResult}</div>;
              }
              if(searchResponse.get('articles').size > 0){
                  articleResult = searchResponse.get('articles').map((article) => {
                    return <p key={article.get('articleId')}><a>{article.get('title')}</a></p>
                  });
                  articleResult = <div className="column"><h3>Articles</h3>{articleResult}</div>;
              }
              if(searchResponse.getIn(['locations', 'areaIsland']).size > 0){
                  areaResult = searchResponse.getIn(['locations', 'areaIsland']).map((area, index) => {
                    return <p key={index}><a onClick={this.handleClick.bind(this, area.get('name'), 'area')}>{area.get('displayString')} (<span>{area.get('count')} hotels</span>)</a></p>
                  });
                  areaResult = <div className="column"><h3>Areas</h3>{areaResult}</div>;
              }
              if(searchResponse.getIn(['locations', 'cityPlace']).size > 0){
                  cityResult = searchResponse.getIn(['locations', 'cityPlace']).map((city, index) => {
                    return <p key={index}><a onClick={this.handleClick.bind(this, city.get('name'), 'city')}>{city.get('displayString')} (<span>{city.get('count')} hotels</span>)</a></p>
                  });
                  cityResult = <div className="column"><h3>Cities</h3>{cityResult}</div>;
              }
              if(searchResponse.getIn(['locations', 'country']).size > 0){
                  countryResult = searchResponse.getIn(['locations', 'country']).map((country, index) => {
                    return <p key={index}><a onClick={this.handleClick.bind(this, country.get('name'), 'country')}>{country.get('displayString')} (<span>{country.get('count')} hotels</span>)</a></p>
                  });
                  countryResult = <div className="column"><h3>Countries</h3>{countryResult}</div>;
              }
              if(searchResponse.getIn(['locations', 'continent']).size > 0){
                  continentResult = searchResponse.getIn(['locations', 'continent']).map((continent, index) => {
                    return <p key={index}><a onClick={this.handleClick.bind(this, continent.get('name'), 'continent')}>{continent.get('displayString')} (<span>{continent.get('count')} hotels</span>)</a></p>
                  });
                  continentResult = <div className="column"><h3>Continents</h3>{continentResult}</div>;
              }
          }
      }
    return (
        <div id="search-wrapper" onFocus={(event) => { this.slideDown()}}  onBlur={(event) => { this.slideUp()}}>
            <div className="search" >
                <input id="search-input" onKeyUp={this.keyUp.bind(this)} onKeyDown={this.keyDown.bind(this)} onChange={(event) => {this.handleChange(event)}} value={this.state.searchVal} onClick={(event) => { this.slideDown()}} type="text" placeholder="Search Hotel, City or Country" />
                <SearchIcons searchIcon={this.props.searchIcon} searchReset={this.resetSearchField.bind(this)}/>
            </div>
            <div className="auto-complete" >
                <div id="search-results" className={this.state.slideDown && this.props.hasResponse ? 'search-result-wrapper active' : 'search-result-wrapper'} onClick={(event) => { this.Blobo()}}>
                    <div className="search-result">
                        {continentResult}
                        {countryResult}
                        {areaResult}
                        {cityResult}
                        {hotelResult}
                        {articleResult}
                        {noResponseMsg}
                    </div>
                </div>
            </div>
        </div>
    );
  }

  keyUp(e){
      if(this.state.activeTypingTimer !== ''){
          clearTimeout(this.state.activeTypingTimer);
      }
      const searchVal = e.target.value;
      this.typingTimer = setTimeout(this.Search.bind(this, e.target.value), 500);
      this.setState({activeTypingTimer: this.typingTimer})
  }
  keyDown(e){
      clearTimeout(this.state.activeTypingTimer);
  }
  Search(val){
      if(val.length >= 3){
          this.props.sgSearch(val);
      }else{
        //   console.log('typ some more yaoo!');
      }
  }
  handleClick(value, type, e){
      e.preventDefault();
      this.setState({preventBlur: true});
      setTimeout(() => {
          this.setState({preventBlur: false});
          this.setState({slideDown: false});
      }, 500);
      // let obj = { location: type, val: value };

      value = value.toLowerCase().replace(' ', '+');

      browserHistory.push('/' + type + '/' + value);

      // const search = Map(obj);
      // this.props.loadSearchFilteredHotels(search);
  }
  slideDown(){
      this.setState({slideDown: true});
  }
  slideUp(){
      if(this.state.slideUpTimer !== ''){
          clearTimeout(this.state.slideUpTimer);
      }
     this.state.slideUpTimer = setTimeout(() => {
          if(!this.state.preventBlur)
              this.setState({slideDown: false});
          }, 200);
  }
  Blobo(){
      this.setState({preventBlur: true});
      setTimeout(() => {
          this.setState({preventBlur: false});
      }, 500);
      document.getElementById('search-input').focus();
  }
  resetSearchField(){
      this.setState({searchVal: ''});
      if(this.props.searchFilter.size !== 0){
          this.props.searchReset();
      }else{
          this.props.setDefaultSearch();
      }

  }
  handleChange(event){
      this.setState({searchVal: event.target.value})
  }
}
