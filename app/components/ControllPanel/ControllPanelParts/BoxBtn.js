import React, { Component } from "react";

export default class BoxBtn extends Component {
    updateView(){
        const view = 'Box';
        this.props.toggleView(view);
    }

  render() {
    return (
      <div className="view-buttons-wrapper">
        <button onClick = {this.updateView.bind(this)} className="boxview-button"></button>
      </div>
    );
  }
}
