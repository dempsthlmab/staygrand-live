import React, { Component } from "react";
// import { Link } from "react-router";
import FilterPanel from './FilterPanel';
import OrderBtn from "./FilterParts/OrderBtn";

export default class BottomPart extends Component {

    render() {
        const btnData = [
            {text: "Highest Score", code: "score"},
            {text: "Most Awards", code: "awards"},
            {text: "Most Praise", code: "opinions"}
        ];
        const Btns = btnData.map((btn, index) => {
            const active = this.props.activeOrder === btn.code ? true : false;
            return <OrderBtn key={index} text={btn.text} active={active} code={btn.code} setOrder={this.props.setOrder}/>
        });
        return (
            <div className="controll-panel-bottom-part">
                <div className="containery full-strech">
                    <div className="controll-panel-bottom-wrapper">
                        <div className="order-bar-wrapper">
                            <div className="order-bar">
                                { Btns }
                            </div>
                        </div>
                        <FilterPanel resetFilter={this.props.resetFilter} filterHotels={this.props.filterHotels} onSectionClick={this.props.onSectionClick} filterBoxCollapse={this.props.filterBoxCollapse} onFilterBtnClick={this.props.onFilterBtnClick} filterCollapseState={this.props.filterCollapseState} Filters={this.props.Filters} activeFilters={this.props.activeFilters} collapseChildren={this.props.collapseChildren}/>
                    </div>
                </div>
            </div>
        );
    }
}
