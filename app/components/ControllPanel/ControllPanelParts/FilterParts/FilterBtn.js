import React, { Component } from 'react';

// import controllable from 'react-controllable';

// @controllable(['filterCollapseState'])
export default class FilterBtn extends Component {
  render() {
      const filterIcon = "/assets/images/icons/filter-icon.svg";
      const activeFilters = this.props.activeFilters;
      let buttonState = '';
      if(activeFilters.get('ratings').size !== 0 || activeFilters.get('claimtofames').size !== 0){
          buttonState = ' active';
      }else if(this.props.filterCollapseState){
          buttonState = ' slidedown';
      }else{
          buttonState = ' slideup';
      }

    return (
        <div onClick={(event) => {this.handleClick(event)}} className={"filter-button"+buttonState}><div className="desktop"><span className="no-filter">Filter ResUlts</span><span className="yes-filter">Filter ActiVe</span></div><div className="mobile"><img src={filterIcon} /></div></div>
    );
  }
  handleClick(event) {
    // Call the `onSelectedTabIndexChange` callback with the new value.
    if (!this.props.onFilterBtnClick) return;
    const index = this.props.filterCollapseState ? false : true;
    this.props.onFilterBtnClick(index);
  }
}
FilterBtn.defaultProps = {
    filterCollapseState: false
};
