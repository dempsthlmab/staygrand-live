import React, { Component } from "react";

export default class RatingBox extends Component {
  render() {
      const isChecked = this.props.ischecked ? " checked" : "";
    return (
        <div className="checkbox-wrapper">
            <input id={"review-check-"+this.props.id} className={"ratingsCheck"+isChecked} type="checkbox" name="ratings[]" value={this.props.code} />
            <label htmlFor={"review-check-"+this.props.id}><span></span><p>{this.props.name}</p></label>
        </div>
    );
  }
}
