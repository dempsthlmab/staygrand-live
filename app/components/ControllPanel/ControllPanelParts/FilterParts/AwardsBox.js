import React, { Component } from "react";

export default class AwardsBox extends Component {
  render() {
      const isChecked = this.props.ischecked ? " checked" : "";
    return (
        <div className="checkbox-wrapper">
            <input id={"award-check-"+this.props.id} className={"awardsCheck"+isChecked} type="checkbox" name="awards[]" value={this.props.code} />
            <label htmlFor={"award-check-"+this.props.id}><span></span><p>{this.props.name}</p></label>
        </div>
    );
  }
}
