import React, { Component } from "react";

export default class OrderBtn extends Component {

    render(){
        return(
            <button className={this.props.active ? "orderbtn active" : "orderbtn"} onClick={(event) => {this.handleClick();}}>{this.props.text}</button>
        );
    }
    handleClick(){
        if(this.props.active){
            this.props.setOrder('default');
        }else{
            this.props.setOrder(this.props.code);
        }
    }
}
