import React, { Component } from "react";

export default class ClaimToFameBox extends Component {
  render() {
      const isChecked = this.props.ischecked ? " checked" : "";
    return (
        <div className="checkbox-wrapper">
            <input id={"ctf-check-"+this.props.id} className={"claimtofamesCheck"+isChecked} type="checkbox" name="claimtofame[]" value={this.props.code} />
            <label htmlFor={"ctf-check-"+this.props.id}><span></span><p>{this.props.name}</p></label>
        </div>
    );
  }
}
