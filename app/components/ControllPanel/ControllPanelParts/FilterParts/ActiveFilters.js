import React, { Component } from "react";

export default class ActiveFilters extends Component {

    compileText(activeFilters){
        let ratings = '', claimtofames = '', awards = '';
        if(activeFilters.get('ratings').size !== 0){
            ratings = <span>Claim To Fames: <span className="pinku"> {activeFilters.get('ratings').size} </span></span>;
        }
        if(activeFilters.get('claimtofames').size !== 0){
            claimtofames = <span>, Collection Members: <span className="pinku"> {activeFilters.get('claimtofames').size} </span></span>;
        }
        // if(activeFilters.get('awards').size !== 0){
        //     awards = <span>, Awards: <span className="pinku"> {activeFilters.get('awards').size} </span></span>;
        // }
        return <p className="filter-tags"><span className="pinku">Active</span> filters: {ratings} {claimtofames}</p>;
    }
  render() {
    //   console.log(this.props);
      const activeFilters = this.props.activeFilters;
      let content = '';
      if(activeFilters.get('ratings').size !== 0 || activeFilters.get('claimtofames').size !== 0){
          content = this.compileText(activeFilters);
      }
    return (
        <div className="filter-panel-result-wrapper">
            <div className={ content !== '' ? 'filter-panel-result active' : 'filter-panel-result' }>
                { content }
            </div>
        </div>
    );
  }
}
