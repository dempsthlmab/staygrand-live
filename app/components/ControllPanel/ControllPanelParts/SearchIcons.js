import React, { Component } from "react";

import DefaultIcon from './SearchIcons/DefaultIcon';
import LoadingIcon from './SearchIcons/LoadingIcon';
import DoneIcon from './SearchIcons/DoneIcon';

export default class SearchIcons extends Component {
    setIcons(searchIcon){
        if(searchIcon === 'default'){
            return(
                <DefaultIcon />
            );
        }else if(searchIcon === 'loading'){
            return(
                <LoadingIcon />
            );
        }else if(searchIcon === 'done'){
            return(
                <DoneIcon searchReset={this.props.searchReset}/>
            );
        }
    }
  render() {
    return (
        <div className="search-icons">
            { this.setIcons(this.props.searchIcon) }
        </div>
    );
  }
}
