import React, { Component } from "react";

import FilterBtn from "./FilterParts/FilterBtn";
import RatingsBox from "./FilterParts/RatingBox";
import ClaimToFameBox from "./FilterParts/ClaimToFameBox";
// import AwardsBox from "./FilterParts/AwardsBox";
import ActiveFilters from "./FilterParts/ActiveFilters";


export default class FilterPanel extends Component {
    render(){
        const activeFilters = this.props.activeFilters;

        const ratings = this.props.Filters.get('ratings');
        const RatingsComponent = ratings.map((rating) => {
            let isChecked = false;
            if(activeFilters.get('ratings').size !== 0){
                isChecked = activeFilters.get('ratings').includes(rating.get('name'));
            }
            return <ClaimToFameBox key={'ctf'+rating.get('claimTypeId')} ischecked={isChecked} id={rating.get('claimTypeId')} name={rating.get('description')} code={rating.get('name')}/>;

            //return <RatingsBox key={'rat'+rating.get('id')} ischecked={isChecked} id={rating.get('id')} name={rating.get('name')} code={rating.get('code')} />;
        });

        const claimtofames = this.props.Filters.get('claimtofames');
        const ClaimToFamesComponent = claimtofames.map((claimtofame) => {
            let isChecked = false;
            if(activeFilters.get('claimtofames').size !== 0){
                isChecked = activeFilters.get('claimtofames').includes(claimtofame.get('name'));
            }
            return <ClaimToFameBox key={'ctf'+claimtofame.get('claimTypeId')} ischecked={isChecked} id={claimtofame.get('claimTypeId')} name={claimtofame.get('description')} code={claimtofame.get('name')}/>;
        });


        return (
            <div className={ this.props.filterCollapseState ? 'filter-panel opened' : 'filter-panel' }>
                <div className="filter-panel-header">
                    <p>
                        {"You are filtering the list view. Try out the map view by clicking the map icon"}
                    </p>
                    <ActiveFilters activeFilters={this.props.activeFilters}/>
                </div>
                <div className="section-wrapper">
                    <button onClick={this.handleClick.bind(this, 'ratingsBox', 'ratings')} className="section-curtain" type="button" name="button"><h3>Claim to Fame</h3><p className={ this.props.filterBoxCollapse.ratings ? 'opened' : 'closed' }>{"TOGGLE SECTION"}</p></button>
                    <div id="ratingsBox" className={ this.props.filterBoxCollapse.ratings ? 'panel-section-wrapper opened' : 'panel-section-wrapper' }>
                        <div className="panel-section">
                            { RatingsComponent.toJS() }
                        </div>
                    </div>
                </div>
                <div className="section-wrapper">
                    <button onClick={this.handleClick.bind(this, 'claimtofameBox', 'claimtofames')} className="section-curtain" type="button" name="button"><h3>collection members</h3><p className={ this.props.filterBoxCollapse.claimtofames ? 'opened' : 'closed' }>{"TOGGLE SECTION"}</p></button>
                    <div id="claimtofameBox" className={ this.props.filterBoxCollapse.claimtofames ? 'panel-section-wrapper opened' : 'panel-section-wrapper' }>
                        <div className="panel-section">
                            { ClaimToFamesComponent.toJS() }
                        </div>
                    </div>
                </div>

                <div className="filter-panel-footer">
                    <div className="panel-btn-wrapper">
                        <button onClick={this.Submitter.bind(this)} id="active-filters" className="panel-btn" type="button" name="button">ACTIVATE FILTER</button>
                        <span className="btn-devider"></span>
                        <button onClick={(event) => { this.props.resetFilter(); this.uncheckChecked();}} id="deactive-filters" className="panel-btn" type="button" name="button">CLEAR FILTER</button>
                    </div>
                    <p>The results will show the highest rated hotels within your selected categories</p>
                </div>
            </div>
        );
    }
    handleClick(collapseTarget, type, event){
        if (!this.props.onSectionClick) return;
        const curtainBox = document.getElementById(collapseTarget);
        let index;
        switch (type) {
            case 'ratings':
                index = this.props.filterBoxCollapse.ratings ? false : true;
                this.handleCollapseHeight(this.props.filterBoxCollapse.ratings, curtainBox);
                break;
            case 'claimtofames':
                index = this.props.filterBoxCollapse.claimtofames ? false : true;
                this.handleCollapseHeight(this.props.filterBoxCollapse.claimtofames, curtainBox);
                break;
            // case 'awards':
            //     index = this.props.filterBoxCollapse.awards ? false : true;
            //     this.handleCollapseHeight(this.props.filterBoxCollapse.awards, curtainBox);
            //     break;
            default:
        }
        this.props.onSectionClick(type, index);
    }
    handleCollapseHeight(collapsed, target){
        let height;
        if(!collapsed){
            height = target.childNodes[0].offsetHeight;
        }else{
            height = 0;
        }
        target.style.height=height+'px';
    }
    Submitter(event){
        let filters = {};
        filters.ratings = [].slice.call(document.querySelectorAll("input.ratingsCheck:checked")).map((rating) =>{
            return rating.value;
        });
        filters.claimtofames = [].slice.call(document.querySelectorAll("input.claimtofamesCheck:checked")).map((claimtofame) => {
            return claimtofame.value;
        });
        // filters.awards = [].slice.call(document.querySelectorAll("input.awardsCheck:checked")).map((awards) => {
        //     return awards.value;
        // });
        this.props.filterHotels(filters);
        this.collapse();
    }
    collapse(){
        if (!this.props.onFilterBtnClick) return;
        const index = this.props.filterCollapseState ? false : true;
        this.props.onFilterBtnClick(index);
    }
    uncheckChecked(){
        let checkboxes = document.getElementsByClassName("checked");
        for(var i =0; i< checkboxes.length; i++){
            var check = checkboxes[i];
            if(!check.disabled){
                check.checked = false;
            }
        }
        this.collapse();
        this.props.collapseChildren();
    }
}
// <div className="section-wrapper">
//     <button onClick={this.handleClick.bind(this, 'awardsBox', 'awards')} className="section-curtain" type="button" name="button"><h3>awards</h3><p className={ this.props.filterBoxCollapse.awards ? 'opened' : 'closed' }>{"TOGGLE SECTION"}</p></button>
//     <div id="awardsBox" className={ this.props.filterBoxCollapse.awards ? 'panel-section-wrapper opened' : 'panel-section-wrapper' }>
//         <div className="panel-section">
//             { AwardsComponent.toJS() }
//         </div>
//     </div>
// </div>
// const awards = this.props.Filters.get('awards');
// const AwardsComponent = awards.map((award) => {
//     let isChecked = false;
//     if(activeFilters.get('awards').size !== 0){
//         isChecked = activeFilters.get('awards').includes(award.get('slug'));
//     }
//     return <AwardsBox key={'aw'+award.get('awardId')} ischecked={isChecked} id={award.get('awardId')} name={award.get('name')} code={award.get('slug')}/>;
// });
