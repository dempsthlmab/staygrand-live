import React, { Component } from "react";
// import { Link } from "react-router";

import TopPart from './ControllPanelParts/TopPart';
import BottomPart from './ControllPanelParts/BottomPart';


export default class Controllpanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterCollapse: false,
            filterBoxCollapse: {
                ratings: false,
                claimtofames: false
                // awards: false
            },
        };
    }

    _onFilterBtnClick(index){
        this.setState({
            filterCollapse: index
        });
    }

    _onFilterSectionClick(type, index){
        let filters = this.state.filterBoxCollapse;
        switch (type) {
            case 'ratings':
                this.setState({
                    filterBoxCollapse: {
                        ratings: index,
                        claimtofames: filters.claimtofames,
                        awards: filters.awards
                    }
                });
                break;
            case 'claimtofames':
                this.setState({
                    filterBoxCollapse: {
                        ratings: filters.ratings,
                        claimtofames: index,
                        awards: filters.awards
                    }
                });
                break;
            // case 'awards':
            //     this.setState({
            //         filterBoxCollapse: {
            //             ratings: filters.ratings,
            //             claimtofames: filters.claimtofames,
            //             awards: index
            //         }
            //     });
            //     break;
            default:
        }
    }

    collapseChildren(){
        this.setState({
            filterBoxCollapse: {
                ratings: false,
                claimtofames: false
                // awards: false
            }
        });
    }



    render() {
        return (
            <div id="controll-panel" className="controll-panel">
                <div>
                    <TopPart loadSearchFilteredHotels={this.props.loadSearchFilteredHotels} searchResponse={this.props.searchResponse} searchIcon={this.props.searchIcon} searchVal={this.props.searchVal} searchFilter={this.props.searchFilter} setDefaultSearch={this.props.setDefaultSearch} searchReset={this.props.searchReset} sgSearch={this.props.sgSearch} viewType={this.props.viewType} filterCollapseState={this.state.filterCollapse} onFilterBtnClick={this._onFilterBtnClick.bind(this)} toggleView={this.props.toggleView} activeFilters={this.props.activeFilters} />
                    <BottomPart resetFilter={this.props.resetFilter} filterHotels={this.props.filterHotels} Filters={this.props.Filters} onSectionClick={this._onFilterSectionClick.bind(this)} onFilterBtnClick={this._onFilterBtnClick.bind(this)} filterBoxCollapse={this.state.filterBoxCollapse} filterCollapseState={this.state.filterCollapse} changeView={this.props.changeView} activeFilters={this.props.activeFilters} collapseChildren={this.collapseChildren.bind(this)} activeOrder={this.props.activeOrder} setOrder={this.props.setOrder} />
                </div>
            </div>
        );
    }
}
