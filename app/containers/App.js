import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

function capitalizeFirstLetter(string) {
    if (!string) {
        return string;
    }

    return string.charAt(0).toUpperCase() + string.slice(1);
}

class App extends Component {
    render() {
        if (this.props.location.pathname.match(/^\/hotel\/(.*)$/)) {
            var title = 'StayGrand | ' + capitalizeFirstLetter(this.props.siteState.search.searchVal);
            var description = capitalizeFirstLetter(this.props.siteState.search.searchVal);
        }
        else {
            var placeName = capitalizeFirstLetter(this.props.siteState.search.searchVal);

            if (this.props.hotels.hotels[0].length === 1) {
                var title = 'StayGrand | ' + placeName;
                var description = 'Discover the best hotel in ' + placeName;
            }
            else {
                var title = this.props.siteState.search.searchVal ? 'StayGrand | ' + placeName : 'StayGrand';
                var description = this.props.siteState.search.searchVal ? 'Discover the best hotels in ' + placeName : 'The World\'s Most Praised Hotels';
            }
        }

        return (
            <div>
                <Helmet
                    htmlAttributes={{"lang": "en"}}
                    title={ description }
                    titleTemplate="StayGrand | %s"
                    defaultTitle="StayGrand"
                    meta={[
                        {name: "description", content: description },
                        {name: "viewport", content: "width=device-width, initial-scale=1"},
                        {name: "theme-color", content: "#ffffff"},
                        {name: "og:title", content: title},
                        {name: "og:description", content: description},
                        {name: "og:url", content: 'https://www.staygrand.com' + this.props.location.pathname},
                        // {name: "og:image", content: 'https://'},
                        // {name: "og:image:width", content: '1200'},
                        // {name: "og:image:height", content: '630'},
                    ]}
                    link={[
                        {rel: "apple-touch-icon", sizes: "180x180", href: "/assets/favicons/apple-touch-icon.png"},
                        {rel: "icon", type: "image/png", href: "/assets/favicons/favicon-32x32.png", sizes: "32x32"},
                        {rel: "icon", type: "image/png", href: "/assets/favicons/favicon-16x16.png", sizes: "16x16"},
                        {rel: "manifest", href: "/assets/favicons/manifest.json"},
                        {rel: "mask-icon", href: "/assets/favicons/safari-pinned-tab.svg", color: "#5bbad5"},
                        {rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Fredoka+One|Herr+Von+Muellerhoff|Limelight|Questrial|Roboto:300,300i,400,700,900"},
                        {rel: "stylesheet", type: "text/css", href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"},
                        {rel: "stylesheet", type: "text/css", href: "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"}
                    ]}
                />
                {this.props.children}
            </div>
        )
    }
}

const mapStateToProps = (state) => {

    return {
        siteState: state.siteState.toJS(),
        hotels: state.hotels.toJS(),
    }
}

export default connect(
    mapStateToProps,
    {

    }
)(App)
