import React, { Component } from 'react';

import { connect } from "react-redux";

import SingleHotelView from "components/layout/SingleHotel";

import {browserHistory} from 'react-router';

import Footer from "components/layout/Footer";

import { loadSingleHotel, setActiveHotel } from 'actions/questions';

import Helmet from 'react-helmet';


class SingleHotel extends Component {

    static fetchData({ store, params, history }) {
        let { slug } = params;
      return store.dispatch(loadSingleHotel(slug));
    }


    // componentWillReceiveProps(nextProps) {
    //     console.log(nextProps);
    //     if (nextProps.location !== this.props.location) {
    //         this.setState({ prevPath: this.props.location });
    //     }
    // }
    componentWillMount() {
        let { singleHotel } = this.props;
        let { slug } = this.props.params;
        if(singleHotel.getIn(['singleHotel', 'slug']) !== slug){
            this.props.loadSingleHotel(slug);
        }
    }

    loadMapView(){
        const { singleHotel } = this.props;
        const singleHotelo = [
            {
                apiUrl: singleHotel.getIn(['singleHotel','apiUrl']),
                avgRating: singleHotel.getIn(['singleHotel','avgRating']),
                cityPlace: singleHotel.getIn(['singleHotel','cityPlace']),
                country: singleHotel.getIn(['singleHotel','country']),
                hotelId: singleHotel.getIn(['singleHotel','hotelId']),
                lat: singleHotel.getIn(['singleHotel','lat']),
                lng: singleHotel.getIn(['singleHotel','lng']),
                name: singleHotel.getIn(['singleHotel','name']),
                numAwards: singleHotel.getIn(['singleHotel','numAwards']),
                numOpinions: singleHotel.getIn(['singleHotel','numOpinions']),
                singleImage: singleHotel.getIn(['singleHotel','images']).first().get('imageUrl'),
                slug: singleHotel.getIn(['singleHotel','slug'])
            }
        ];
        this.props.setActiveHotel(singleHotelo);
        browserHistory.push('/');
    }


  render() {
      let { singleHotel } = this.props;
      let { slug } = this.props.params;
      if(singleHotel.get('singleHotel').size === 0){
          return false;
      }else if(singleHotel.getIn(['singleHotel', 'slug']) !== slug){
          return false;
      }
      const hotelName = singleHotel.getIn(['singleHotel', 'name']);
    return (
        <div>
            <Helmet
                title={hotelName}
            />
            <SingleHotelView Hotel={singleHotel.get('singleHotel')} loadMap={this.loadMapView.bind(this)} ratings={singleHotel.get('ratings')}/>
            <Footer location={this.props.location.pathname} />
        </div>
    );
  }
}
function mapStateToProps(state) {
  return { singleHotel: state.singleHotel }
}

export default connect(mapStateToProps, { loadSingleHotel, setActiveHotel })(SingleHotel)
