import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

import Header from "components/layout/Header";
import Footer from "components/layout/Footer";
import Controllpanel from "components/ControllPanel/Controllpanel";
import SiteLoader from "components/layout/SiteLoader";
//
import BoxView from "components/layout/BoxView";
import MapView from "components/layout/GMap";
import LoadingNew from "components/layout/LoadingNew";

import TotalHotels from "components/layout/TotalHotels";

import { resetFilters, fetchHotels, fetchSearchFilter, fetchSearchResult, loadinitHotels, resetSearch, filterHotels, fetchAllMapHotels, fetchMapHotelsInBounds, changeView, resetActiveHotel, setOrderPar, setDefaultSearch, getSubData, fetchMapHotel, resetMapHotel } from 'actions/questions';

import { fitBounds } from 'google-map-react/utils';

import { Map } from 'immutable';

class Welcome extends Component {

    static fetchData({ store, params, route }) {
        let slug = params.slug;
        let type = route.searchType;

        let search = Map({ location: type, val: slug });

        const { siteState } = store.getState();
        const filters = siteState.get('activeFilter').toJS();
        const order = siteState.get('activeOrder');

        return store.dispatch(fetchSearchFilter(filters, search, order))
            .then(() => store.dispatch(getSubData()));
    }

    componentWillMount() {
        // let { hotels, mapHotels } = this.props;

        // if (this.props.route.searchType) {
        //     let slug = this.props.params.slug;
        //     let type = this.props.route.searchType;

        //     let search = Map({ location: type, val: slug });

        //     this.loadSearchFilteredHotels(search);
        // }
        // else {
        //     if (hotels) {
        //         if (hotels.get('hotels').size === 0){
        //             this.props.loadinitHotels();
        //         }
        //         // if (mapHotels.get('hotels').size === 0){
        //         //     this.props.fetchAllMapHotels();
        //         // }
        //     }
        // }

        // this.props.getSubData();
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.route.searchType != this.props.route.searchType || nextProps.params.slug != this.props.params.slug) {
            if (nextProps.route.searchType) {
                let slug = nextProps.params.slug;
                let type = nextProps.route.searchType;

                let search = Map({ location: type, val: slug });

                this.loadSearchFilteredHotels(search);
            }
            else {
                this.props.loadinitHotels();
                // this.props.fetchAllMapHotels();

                this.setDefaultSearch();

                // let { hotels, mapHotels } = this.props;

                // if(hotels.get('hotels').size === 0){
                //     this.props.loadinitHotels();
                // }
                // if(mapHotels.get('hotels').size === 0){
                //     this.props.fetchAllMapHotels();
                // }
            }
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { siteState } = this.props;
        const newSiteState = nextProps.siteState;
        let update;
        if(siteState.get('activeHotel') !== null && newSiteState.get('activeHotel') === null){
            update = false;
        }else{
            update = true;
        }
      // You can access `this.props` and `this.state` here
      // This function should return a boolean, whether the component should re-render.
      return update;
    }

    resetFilters(){
        const { siteState } = this.props;
        let search = siteState.getIn(['search', 'searchFilter']);
        const order = siteState.get('activeOrder');
        this.props.resetFilters(search, order);
    }

    //Dispatches the search action and returns the results to the dropdown
    sgSearch(searchVal){
        const { siteState } = this.props;
        const filters = siteState.get('activeFilter').toJS();
        this.props.fetchSearchResult(searchVal, filters);
    }

    resetSearchResponse(){
        const { siteState } = this.props;
        const filters = siteState.get('activeFilter').toJS();
        const order = siteState.get('activeOrder');
        this.props.resetSearch(filters, order);
    }

    setDefaultSearch(){
        this.props.setDefaultSearch();
    }

    //Changes the view
    toggleView(newView) {
        this.props.changeView(newView);
    }
    //Gets the next set of hotels
    LoadMoreHotels(){
        let { hotels } = this.props;
        const nexer = hotels.get('next_page');
        this.props.fetchHotels(nexer);
    }
    //Load hotels on what you have filtered on
    loadFilteredHotels(filters){
        const { siteState } = this.props;
        const search = siteState.getIn(['search', 'searchFilter']);
        const order = siteState.get('activeOrder');
        this.props.filterHotels(filters, search, order);
    }
    //If a search result that is not a hotel is clicked the hotels gets loaded that matches the filter that is clicked
    loadSearchFilteredHotels(search){
        const { siteState } = this.props;
        const filters = siteState.get('activeFilter').toJS();
        const order = siteState.get('activeOrder');
        this.props.fetchSearchFilter(filters, search, order);
    }

    setOrderPar(order){
        const { siteState } = this.props;
        const search = siteState.getIn(['search', 'searchFilter']);
        const filters = siteState.get('activeFilter').toJS();
        this.props.setOrderPar(filters, search, order);
    }

    removeActiveHotel(){
        this.props.resetActiveHotel();
    }

    // enableScrollOnMap(e){
    //     if(e.touches.length < 2){
    //         e.stopPropagation();
    //     }
    // }

    viewTypRender(){
        let { siteState, hotels, mapHotels } = this.props;
        const view = siteState.get('view');
        let viewType = '';

        if (view === 'Box') {
            if (hotels.get('loadingNew')) {
                viewType = <LoadingNew />;
            } else {
                const nextUrlExists = hotels.get('next_page') !== null ? true : false;
                const activeRatings = siteState.getIn(['activeFilter', 'ratings']);
                viewType = <BoxView Hotels={hotels.get('hotels')} loadMoreFunction={this.LoadMoreHotels.bind(this)} nextUrlExists={nextUrlExists} isLoadingMore={hotels.get('loadingMore')} activeRatings={activeRatings}/>;
            }
        } else if (view === 'Map') {

            // if (siteState.get('activeHotel') === null && mapHotels.get('loadingNew')) {
            //     viewType = <LoadingNew />;
            // } else {
                let mapItems;
                let mapSettings;

                if (siteState.get('activeHotel') !== null && mapHotels.get('hotels').size === 0) {
                    mapItems = siteState.get('activeHotel');
                    mapSettings = {
                        zoom: 17,
                        center: {
                            lat: mapItems.first().get('lat'),
                            lng: mapItems.first().get('lng'),
                        },
                        activeMarker: "1_"+mapItems.first().get('hotelId')+"",
                    };

                    /*if (mapHotels.get('hotels').size !== 0) {
                        this.removeActiveHotel();
                    }*/
                }
                else {
                    mapItems = hotels.get('hotels').first();

                    var nw = { lat:-9999, lng:9999 };
                    var se = { lat:9999, lng: -9999 };

                    for (var i = 0; i < mapItems.size; i++) {
                        const hotel = mapItems.get(i);

                        if (hotel.get('lat') > nw.lat) nw.lat = hotel.get('lat');
                        if (hotel.get('lng') < nw.lng) nw.lng = hotel.get('lng');

                        if (hotel.get('lat') < se.lat) se.lat = hotel.get('lat');
                        if (hotel.get('lng') > se.lng) se.lng = hotel.get('lng');
                    }

                    const size = {
                      width: window.innerWidth, // Map width in pixels
                      height: window.innerHeight, // Map height in pixels
                    };

                    let { center, zoom } = fitBounds({ nw, se }, size);

                    if (mapItems.size == 1) {
                        const hotel = mapItems.first();
                        zoom = 12;

                        center = {
                            lat: hotel.get('lat'),
                            lng: hotel.get('lng'),
                        };
                    }

                    mapSettings = {
                        zoom: zoom > 0 ? zoom : 1,
                        center: {
                            lat: center.lat,
                            lng: center.lng,
                        },
                        activeMarker: -1,
                    };

                    // if (mapHotels.get('popupHotel')) {
                    //     mapSettings.center = {
                    //         lat: mapHotels.get('popupHotel').get('lat'),
                    //         lng: mapHotels.get('popupHotel').get('lng')
                    //     }
                    // }
                }

                /*if (siteState.get('activeHotel') !== null && mapHotels.get('hotels').size === 0) {
                    mapItems = siteState.get('activeHotel');
                    mapSettings = {
                        zoom: 17,
                        center: {
                            lat: mapItems.first().get('lat'),
                            lng: mapItems.first().get('lng'),
                        },
                        activeMarker: "1_"+mapItems.first().get('hotelId')+"",
                    };

                } else if(siteState.get('activeHotel') !== null && mapHotels.get('hotels').size !== 0) {
                    mapItems = mapHotels.get('hotels');
                    const activeHotel = siteState.get('activeHotel');
                    mapSettings = {
                        zoom: 17,
                        center: {
                            lat: activeHotel.first().get('lat'),
                            lng: activeHotel.first().get('lng'),
                        },
                        activeMarker: "1_"+activeHotel.first().get('hotelId')+"",
                    };
                    this.removeActiveHotel();
                } else {
                    mapItems = mapHotels.get('hotels');

                    var nw = { lat:-9999, lng:9999 };
                    var se = { lat:9999, lng: -9999 };

                    for (var i = 0; i < mapItems.size; i++) {
                        const hotel = mapItems.get(i);

                        if (hotel.get('lat') > nw.lat) nw.lat = hotel.get('lat');
                        if (hotel.get('lng') < nw.lng) nw.lng = hotel.get('lng');

                        if (hotel.get('lat') < se.lat) se.lat = hotel.get('lat');
                        if (hotel.get('lng') > se.lng) se.lng = hotel.get('lng');
                    }

                    const size = {
                      width: window.innerWidth, // Map width in pixels
                      height: window.innerHeight, // Map height in pixels
                    };

                    let { center, zoom } = fitBounds({ nw, se }, size);

                    if (mapItems.size == 1) {
                        const hotel = mapItems.first();
                        zoom = 12;

                        center = {
                            lat: hotel.get('lat'),
                            lng: hotel.get('lng'),
                        };
                    }

                    mapSettings = {
                        zoom: zoom > 0 ? zoom : 1,
                        center: {
                            lat: center.lat,
                            lng: center.lng,
                        },
                        activeMarker: -1,
                    };

                }*/

                viewType = (
                    <div className="cluster-map">
                        <MapView hotels={mapHotels.get('hotels')} loadingNew={mapHotels.get('loadingNew')} fetchMapHotel={this.props.fetchMapHotel} resetMapHotel={this.props.resetMapHotel} fetchMapHotelsInBounds={this.props.fetchMapHotelsInBounds} popupHotel={mapHotels.get('popupHotel')} zoom={mapSettings.zoom} center={mapSettings.center}/>
                    </div>);
            // }

        }

        return viewType;
    }

    isLoadingPage(){
        let { hotels, mapHotels, siteState } = this.props;
        if(hotels.get('hotels').size === 0 && siteState.get('activeHotel') === null){
            return (
                <SiteLoader />
            );
        }else{
            // console.log(hotels);

            return(
                <div>
                  <Header />
                  <Controllpanel loadSearchFilteredHotels={this.loadSearchFilteredHotels.bind(this)} searchResponse={siteState.getIn(['search', 'searchCategorys'])} searchIcon={siteState.getIn(['search', 'searchIcon'])} searchVal={siteState.getIn(['search', 'searchVal'])} searchFilter={siteState.getIn(['search', 'searchFilter'])} searchReset={this.resetSearchResponse.bind(this)} sgSearch={this.sgSearch.bind(this)} setDefaultSearch={this.setDefaultSearch.bind(this)} resetFilter={this.resetFilters.bind(this)} filterHotels={this.loadFilteredHotels.bind(this)} viewType={siteState.get('view')} toggleView={this.toggleView.bind(this)} Filters={siteState.get('filters')} activeFilters={siteState.get('activeFilter')} activeOrder={siteState.get('activeOrder')} setOrder={this.setOrderPar.bind(this)}/>
                  <TotalHotels total={hotels.get('totalHotels')} singleHotel={this.props.location.pathname.match(/^\/hotel\/(.*)$/) ? true : false} />
                  { this.viewTypRender() }
                  <Footer location={this.props.location.pathname} />
                </div>
            );
        }
    }

  render() {
      const { siteState } = this.props;
    return (
        <div>
            { this.isLoadingPage() }
        </div>

    );
  }
}
function mapStateToProps(state) {
  return { hotels: state.hotels, mapHotels: state.mapHotels, siteState: state.siteState }
}

export default connect(mapStateToProps, { resetFilters, fetchHotels, fetchSearchFilter, fetchSearchResult, loadinitHotels, resetSearch, filterHotels, fetchAllMapHotels, fetchMapHotelsInBounds, changeView, resetActiveHotel, setOrderPar, setDefaultSearch, getSubData, fetchMapHotel, resetMapHotel })(Welcome)
