import axios from "axios";
import qs from "qs";

var base_url = 'https://cms.staygrand.com';
//var base_url = 'http://cms.staygrand.test';

export function loadinitHotels() {
  return {
    type: 'GET_INIT_HOTELS',
    // type2: 'GET_INIT_STATE',
    beforeType: true,
    callType: 'setInitHotels',
    promise: axios.get(base_url + "/api/hotels?per_page=20")
  };
}

export function getSubData() {
    return {
        type: 'GET_INIT_STATE',
        beforeType: true,
        callType: 'setInitData',
        promise: axios.get(base_url + "/api/subdata/newAll")
    };
}

//Loads more hotels on box view
export function fetchHotels(url) {
    const API_URL = url;
    return {
      type: 'GET_MORE_HOTELS',
      beforeType: true,
      callType: 'loadMore',
      promise: axios.get(API_URL)
    };
}

/*---------------Search Function and Filter Function For Search Location-----------------*/

//Filtering by search result that is not singleHotel links
export function fetchSearchFilter(filters, search, order) {
    const API_URL = setHotelsUrl(filters, search, order);
    return {
      type: 'GET_SEARCH_FILTERED_HOTELS',
      type2: 'SET_ACTIVE_LOCATION_FILTER',
      // maps: true,
      beforeType: true,
      search: search,
      callType: 'searchFilter',
      promise: axios.all([
          axios.get(API_URL.boxs),
          // axios.get(API_URL.maps),
      ])
    };
}

//resets the search result
export function resetSearch(filters, order){
    const API_URL = setHotelsUrl(filters, false, order);
    return {
      type: 'GET_FILTER_HOTELS',
      type2: 'RESET_SEARCH_FILTER',
      maps: true,
      beforeType: true,
      callType: 'resetSearch',
      promise: axios.all([
          axios.get(API_URL.boxs),
          // axios.get(API_URL.maps),
      ])
    };
}

export function setDefaultSearch(){
    return {
        type: 'SET_DEFAULT_SEARCH',
    };
}

//Makes a search on the typed value
export function fetchSearchResult(searchVal, activeFilters) {
    const API_URL = setSearchUrl(searchVal, activeFilters);
    return {
      type: 'SEARCH_RESULT',
      beforeType: true,
      callType: 'searchResponse',
      promise: axios.get(API_URL)
    };
}



/*-----------Category Filter Functions-----------*/

//Gets hotels that matches choosen filters
export function filterHotels(filters, search, order){
    const API_URL = setHotelsUrl(filters, search, order);
    return {
      type: 'GET_FILTER_HOTELS',
      type2: 'GET_ACTIVE_FILTERS',
      maps: true,
      beforeType: true,
      callType: 'filterHotels',
      filters: filters,
      promise: axios.all([
          axios.get(API_URL.boxs),
          axios.get(API_URL.maps),
      ])
    };
}

//Gets the init hotels with no filtering
export function resetFilters(search, order){
    const API_URL = setHotelsUrl(false, search, order);
    return {
      type: 'RESET_INIT_HOTELS',
      type2: 'RESET_FILTERS',
      maps: true,
      beforeType: true,
      callType: 'resetFilters',
      promise: axios.all([
          axios.get(API_URL.boxs),
          axios.get(API_URL.maps),
      ])
    };
}

//Fetches mapHotels
export function fetchAllMapHotels(){
    const API_URL = base_url + '/api/map-hotels';
    return {
      type: 'GET_ALL_MAP_HOTELS',
      beforeType: true,
      callType: 'fetchMapHotels',
      promise: axios.get(API_URL)
    };
}

export function fetchMapHotelsInBounds(nw, se){
    const API_URL = base_url + '/api/map-hotels-in-bounds';

    return {
      type: 'GET_MAP_HOTELS_IN_BOUNDS',
      beforeType: true,
      callType: 'fetchMapHotelsInBounds',
      promise: axios.get(API_URL + '?nw_lat=' + nw.lat + '&nw_lng=' + nw.lng + '&se_lat=' + se.lat + '&se_lng=' + se.lng)
    };
}

export function fetchMapHotel(slug) {
    const url = base_url + "/api/hotels/"+slug+"";
      return {
        type: 'GET_POPUP_HOTEL',
        beforeType: false,
        callType: 'fetchMapHotel',
        promise: axios.get(url)
      };
}
export function resetMapHotel() {
    return {
        type: 'RESET_POPUP_HOTEL'
    };
}

//Changes the view
export function changeView(newView){
    return {
        type: 'TOGGLE_START_VIEW',
        res: newView
    };
}

/*---------------Hotel Ordering Functions----------------*/

export function setOrderPar(filters, search, order) {
    let API_URL;
    if(order !== 'default'){
        API_URL = setHotelsUrl(filters, search, order);
    }else{
        API_URL = setHotelsUrl(filters, search, false);
    }
    return {
        type: 'GET_ORDERED_HOTELS',
        type2: 'SET_ACTIVE_ORDER',
        beforeType: true,
        order: order,
        callType: 'orderhotels',
        promise: axios.get(API_URL.boxs)
    };
}

/*---------------Single Hotel Actions----------------*/

//Loads init single hotels
export function loadSingleHotel (slug) {
    const url = base_url + "/api/hotels/"+slug+"";
      return {
        type: 'GET_SINGLE_HOTEL',
        // type2: 'GET_INIT_STATE_SINGLE',
        beforeType: false,
        callType: 'setSingleInitState',
        promise: axios.all([
            axios.get(url),
            axios.get(base_url + "/api/ratings")
        ])
      };
}
export function setActiveHotel (singleHotelProps) {
    return {
        type: 'SET_ACTIVE_HOTEL',
        res: singleHotelProps
    };
}
export function resetActiveHotel () {
    return {
        type: 'RESET_ACTIVE_HOTEL',
        res: 'reset'
    };
}


/*-------------Functional Functions------------*/

function setHotelsUrl(filters, search, order){
    let urls = { maps: base_url + "/api/map-hotels", boxs: base_url + "/api/hotels?per_page=20"};

    if(search){
        if(search.size > 0){
            if(strHasGetStart(urls.maps)){
                urls.maps += '&';
            }else{
                urls.maps += '?';
            }
            urls.boxs += '&';
            urls.maps += search.get('location')+'='+search.get('val');
            urls.boxs += search.get('location')+'='+search.get('val');
        }
    }

    if(filters){
        if(filters.ratings.length > 0){
            if(strHasGetStart(urls.maps)){
                urls.maps += '&';
            }else{
                urls.maps += '?';
            }
            urls.boxs += '&';
            urls.maps += qs.stringify({ratings: filters.ratings});
            urls.boxs += qs.stringify({ratings: filters.ratings});
        }
        if(filters.claimtofames.length > 0){
            if(strHasGetStart(urls.maps)){
                urls.maps += '&';
            }else{
                urls.maps += '?';
            }
            urls.boxs += '&';
            urls.maps += qs.stringify({claimtofames: filters.claimtofames});
            urls.boxs += qs.stringify({claimtofames: filters.claimtofames});
        }
        // if(filters.awards.length > 0){
        //     urls.maps += '&';
        //     urls.boxs += '&';
        //     urls.maps += qs.stringify({awards: filters.awards});
        //     urls.boxs += qs.stringify({awards: filters.awards});
        // }
    }
    if(order){
        urls.boxs += '&';
        urls.boxs += 'order_by='+order;
    }
    return urls;
}

function setSearchUrl(searchVal, filters){
    let url = base_url + "/api/search/autocomplete?q="+searchVal+"";

    if(filters){
        if(filters.ratings.length > 0){
            url += '&';
            url += qs.stringify({ratings: filters.ratings});
        }
        if(filters.claimtofames.length > 0){
            url += '&';
            url += qs.stringify({claimtofames: filters.claimtofames});
        }
        // if(filters.awards.length > 0){
        //     url += '&';
        //     url += qs.stringify({awards: filters.awards});
        // }
    }
    return url;
}

function strHasGetStart(str){
    let exists = false;
    if(str.indexOf('?') > -1){
        exists = true
    }
    return exists;
}
