import { camelizeKeys } from 'humps';
import axios from 'axios';

export default function promiseMiddleware() {
  return next => action => {
    const { promise, type, type2, maps, callType, filters, order, search, beforeType, ...rest } = action;

    if (!promise) return next(action);

    const SUCCESS = type;
    const REQUEST = type + '_REQUEST';
    const FAILURE = type + '_FAILURE';
    let SUCCESS2;
    let REQUEST2;
    let FAILURE2;
    let SUCCESSMAPS;
    let REQUESTMAPS;
    let FAILUREMAPS;
    if(type2){
        SUCCESS2 = type2;
        REQUEST2 = type2 + '_REQUEST';
        FAILURE2 = type2 + '_FAILURE';
    }
    if(maps){
        SUCCESSMAPS = type + '_MAPS';
        REQUESTMAPS = type + '_MAPS_REQUEST';
        FAILUREMAPS = type + '_MAPS_FAILURE';
    }
    if(beforeType){
        if(maps){
            next({ ...rest, type: REQUEST });
            next({ ...rest, type: REQUESTMAPS });
        }else{
            next({ ...rest, type: REQUEST });
        }
    }

    // return promise
        switch(callType){

            case 'loadMore':
                return promise
                .then(response => {
                    const res = camelizeKeys(response.data);
                    next({ ...rest, res, type: SUCCESS });
                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'orderhotels':
                return promise
                .then(response => {
                    const res = camelizeKeys(response.data);
                    console.log('Got Repsonse');
                    next({ ...rest, res, type: SUCCESS });
                    next({ ...rest, order, type: SUCCESS2 });
                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'searchFilter':
                return promise
                .then(axios.spread((resp1, resp2) => {
                    const res1 = camelizeKeys(resp1.data);
                    // const res2 = camelizeKeys(resp2.data);
                    const res3= search;
                    next({ ...rest, res1, type: SUCCESS });
                    // next({ ...rest, res2, type: SUCCESSMAPS });
                    next({ ...rest, res1, /*res2,*/ res3, type: SUCCESS2 });
                }))
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'resetSearch':
                return promise
                .then(axios.spread((resp1, resp2) => {

                    const res1 = camelizeKeys(resp1.data);
                    // const res2 = camelizeKeys(resp2.data);
                    const res3 = 'reset';
                    next({ ...rest, res1, type: SUCCESS });
                    // next({ ...rest, res2, type: SUCCESSMAPS });
                    next({ ...rest, res3, type: SUCCESS2 });
                }))
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'searchResponse':
                return promise
                .then(response => {
                    const res = camelizeKeys(response.data);
                    next({ ...rest, res, type: SUCCESS });
                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'filterHotels':
                return promise
                .then(axios.spread((resp1, resp2) => {

                    const res1 = camelizeKeys(resp1.data);
                    const res2 = camelizeKeys(resp2.data);
                    const res3 = camelizeKeys(filters);
                    next({ ...rest, res1, type: SUCCESS });
                    // next({ ...rest, res2, type: SUCCESSMAPS });
                    next({ ...rest, res3, type: SUCCESS2 });
                }))
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'resetFilters':
                return promise
                .then(axios.spread((resp1, resp2) => {

                    const res1 = camelizeKeys(resp1.data);
                    // const res2 = camelizeKeys(resp2.data);
                    const res3 = 'reset';
                    next({ ...rest, res1, type: SUCCESS });
                    // next({ ...rest, res2, type: SUCCESSMAPS });
                    next({ ...rest, res3, type: SUCCESS2 });
                }))
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'fetchMapHotels':
                return promise
                .then(response => {
                    const res = camelizeKeys(response.data);
                    next({ ...rest, res, type: SUCCESS });
                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'fetchMapHotelsInBounds':
                return promise
                .then(response => {
                    const res = camelizeKeys(response.data);
                    next({ ...rest, res, type: SUCCESS });
                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'fetchMapHotel':
                return promise
                .then(response => {
                    const res = camelizeKeys(response.data);
                    next({ ...rest, res, type: SUCCESS });
                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'setSingleInitState':
                return promise
                .then(axios.spread((res1, res2) => {
                    const resp1 = camelizeKeys(res1.data);
                    const resp2 = camelizeKeys(res2.data);

                    let res = { resp1, resp2 };
                    // let resi = { resp2 };
                    // res = camelizeKeys(res);
                    // resi = camelizeKeys(resi);
                    next({ ...rest, res, type: SUCCESS });
                    // next({ ...rest, resi, type: SUCCESS2 });


                    return true;
                }))
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log(error);
                  return false;
                });

            case 'setInitHotels':
                return promise
                .then(response => {
                    let res = camelizeKeys(response.data);
                    // let resi = camelizeKeys(res2.data);
                    // next({ ...rest, resi, type: SUCCESS2 });
                    next({ ...rest, res, type: SUCCESS });

                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log('error');
                  return false;
                });

            case 'setInitData':
                return promise
                .then(response => {
                    let res = camelizeKeys(response.data);

                    next({ ...rest, res, type: SUCCESS });

                    return true;
                })
                .catch(error => {
                  next({ ...rest, error, type: FAILURE });

                  // Another benefit is being able to log all failures here
                  console.log('error');
                  return false;
                });

        }

   };
}
