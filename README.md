
## Features:
- Universal rendering, with async data support
- Server side redirect
- Separate vendor and app js files
- Use [Immutable](https://facebook.github.io/immutable-js/) as store data

## Stack:
- React 15.4.1
- React-Router 2.8.1
- Redux 3.0.0
- Immutable
- Express as isomorphic server
- Babel
- Webpack


## Diffrent packages that are used and their docs

-- google-map-react:
    docs: https://github.com/istarkov/google-map-react
    used for: creating a google map

-- recompose:
    docs: https://github.com/acdlite/recompose
    used for: recomposeing the props for the google map so that the right clusters are showed.

-- slick image slider:
    docs: https://github.com/akiran/react-slick
    used for: all the image carousels on the site are made with this library

-- points-cluster:
    docs: https://github.com/mapbox/supercluster
    used for: creating google maps custers.

-- http-auth:
        docs: https://github.com/http-auth/http-auth
        used for: Setting a http authorization on the live site. It is active if the NODE_ENV=production

-- axios:
    docs: https://github.com/mzabriskie/axios
    used for: Axios makes all the api calls

-- humps
    docs: https://github.com/domchristie/humps
    used for: converting the names for the returned data to capitalised letter if there is hyphen or underscore

-- ejs
    docs: https://github.com/mde/ejs
    used for: template engeine for the server index.js file.

-- qs
    docs: https://github.com/ljharb/qs
    used for: creating query string to the CMS api.



## Routes Draft:
- Intro page: `{base_url}`
- Single Hotel Page: `{base_url}/hotel/{slug}`


### Assets Management

When handling static assets, we want to achieve the following goals:

- Make assigning assets url a no-brainer
- Apply revision when deploying to production to integrate with CDN services easily

The usage example can be found in `[Intro Container](https://github.com/mz026/universal-redux-template/blob/master/app/containers/Intro.js)`


We achieve this by:

First, creating an assets container folder: `app/assets`

### In development mode:

Assign static folder linking `/assets` to the folder above

### In production mode:

Use a gulp task (`gulp build`) to handle it:

- A set of `[rev](https://github.com/smysnk/gulp-rev-all)-ed` assets with hash code appended would be built into `dist/public/assets`
- A static middleware mapping root url to the folder mentioned above is configured in `server.js`
- A set of `[revReplace](https://github.com/jamesknelson/gulp-rev-replace)-ed` server code would be built into `dist/server-build`, so that the rev-ed assets can be used when doing server rendering



## Vendor Scripts:

Vendor related scripts are bundled into a `vendor.js`,
associated settings can be found in the `entry` field of `webpack.config.js`.


## Deployment:

To deploy this app to production environment:

- Prepare a server with NodeJS environment

- Use whatever tool to upload this app to server. ([Capistrano](http://capistranorb.com/) is a good choice.)

- Run `$ NODE_ENV=production npm install` on server
  - After the installation above, `postinstall` script will run automatically, building front-end related assets and rev-ed server code under `dist/` folder.

- Kick off the server with:

` NODE_ENV=production NODE_PATH=./dist/server-build node dist/server-build/server`


## Deployment on Jelastic:

To deploy this app to production environment:

- Run `npm run build` to compile a new dist folder.

- If you have not added any new packeages

    - Just remove the old dist file from the server and upload the new one.

-If you have added new packages

    - Remove all scripts in the package.json file except for the start script.

    - Upload the package.json, username.htpasswd and the dist folder to the ROOT folder through SFTP.

    - SSH into the node and run `npm install`

    - Restart the node and then it should be Ay Okey.


## App Structure

-- actions/questions.js
    This is where all the Api call function's are.

-- components/ControllPanel
    This folder hold all the components for the controll panel on the start page.

-- components/layout
    This folder hold all components for both start page and single hotel view. There are some components that are not being used because of change in the site structure.

    The GMap.js component uses a library called recompose. Link to docs are at the top of this page.

-- containers/App.js
    The App.js component set all the meta tags for the site.

-- containers/Welcome.js
    This component is the landing page component.

-- containers/SingleHotel.js
    This component is the single hotel page component.

-- middleware/promiseApi.js
    This file handles all the returned api calls data from the CMS and fires the right action type so that the
    right reducers get the right data.

-- reducers/
    -- hotels.js
        This reducer holds all the data for the hotels in the box view on the landing page

    -- mapHotels.js
        This reducer holds all the data for the hotels in the map view on the landing page

    -- singleHotel.js
        This reducer holds all the data for the hotel on the single hotel page

    -- siteState.js
        This reducer holds all the data for all the other data then the hotel data.
        T.ex. Search value, active filters etc etc.

    -- index.js
        This file combineds all the reducers so you can get the data in the component.

-- routes/index.js
    This is the routes file for the sites routes

-- server/
    -- server.js
        This is the Express server file that server the right files and data if you are coming to the
        single hotel page from another anything other then the landing page.

    -- views/index.ejs
        This is the index file that renders out the data and component from the server so that React can
        hook on to them when React is loaded on the site.

-- store/configureStore.js
    This is the configure file for the store. Here we add the diffrent middlewares we want and etc.


-- styles/
    I this folder all the style files are located. The app does not hot reload in development when changing this
    but it automatically compile to the dist file.


-- app.js
    The root file.
